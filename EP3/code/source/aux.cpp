using namespace std;
#include <aux.hpp>

bool checkFile(string fileName) {
	fstream file(fileName);

	if (file.fail()) {
		file.open(fileName, fstream::in | fstream::out | fstream::app);
		file.close();
		return false;
	}

	file.close();
	return true;
}

string getTimeStamp() {
	// current date/time based on current system
	time_t now = time(0);

	// convert now to string form
	char* dt = ctime(&now);
	string result = dt;
	return result.substr(0, 24);
}

string trim(string rawString) {
    stringstream withoutTrim;
    withoutTrim << rawString;
    getline(withoutTrim, rawString, ' ');
    return rawString;
}

int getFileSize(fstream& file) {
	int fileSize;
	file.seekg(0, file.end);
	fileSize = (int)file.tellg();
	file.seekg(0);
	return fileSize;
}

void printSize(int size) {
	string labels[] = {" bytes ", " Kb", " Mb"};
	
	int count = 0;
	double formatedSize = (double)size;
	while (formatedSize >= 1000) {
		formatedSize /= 1000;
		count++;
	}
	if (count == 0) 
		cout << setfill(' ') << setw(3) << (int)formatedSize << labels[count];
	else 
		cout << fixed << setprecision(3) << setfill(' ') << setw(7) << formatedSize << labels[count];
}