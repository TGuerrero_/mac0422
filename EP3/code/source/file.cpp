using namespace std;
#include <file.hpp>

void fillBlock(fstream & file, int blocksBase, int block) {
	file.seekp(blocksBase + BLOCK_SIZE*block);
	file << "?";
	for (int i = 0; i < BLOCK_SIZE - 1; i++)
		file << " ";
	file.seekp(blocksBase + BLOCK_SIZE*block);
}

bool findSpaceForMetadata(fstream & file, vector<int>&FAT, vector<bool>&bitmap, int block, int blocksBase) {
	file.seekp(blocksBase + block*BLOCK_SIZE + 1);
	file.seekg(blocksBase + block*BLOCK_SIZE + 1);
	
	char current;
	while ((current = file.get()) != ' ' && current != '@') {
		if (current == '?' || current == EOF) {
			//Resize
			int newBlock = searchFreeBlock(bitmap);
			if (newBlock == - 1)
				return true;
			else {
				FAT[block] = newBlock;
				FAT[newBlock] = -1;
				block = newBlock;
				fillBlock(file, blocksBase, block);
				file.seekp((int)file.tellp() + 1);
			}
		}
		else
			file.seekp((int)file.tellp() + METADATA_SIZE - 1);
	}
	file.seekp((int)file.tellp() - 1);
	return false;
}

void putMetadata(fstream & file, string name, int size, int block) {
	file << name;
	for (int i = 0; i < (int)(MAX_NAME_SIZE - name.length()); i++)
		file << " ";

	file << "|"<<size;
	for (int i = 0; i < DIGITS_FILESIZE - (int)to_string(size).length(); i++)
		file << " ";

	file << "|" << getTimeStamp() << "|";
	file << getTimeStamp() << "|";
	file << getTimeStamp() << "|";
	string blockS = to_string(block);
	file << block;
	for (int i = 0; i < (DIGITS_BLOCKADDRESS - (int)blockS.length()); i++) {
		file << ' ';
	}
	file << "|";
}

void changeTimeStamp(fstream & file) {
	file << getTimeStamp() << "|";
}

int searchFile(fstream & file, int blocksBase, vector<int>&FAT, string fileName, int curBlock, int change) {
	string currentDir;
	int nextBlock = blocksBase + curBlock*BLOCK_SIZE + BLOCK_SIZE;
	file.seekp(blocksBase + BLOCK_SIZE*curBlock + 1);

	getline(file, currentDir, '|');
	currentDir = trim(currentDir);
	while (currentDir != fileName) {
		if (((int)file.tellp() + (METADATA_SIZE - (MAX_NAME_SIZE + 1))) >= nextBlock) {
			// Fim de bloco
			if (FAT[curBlock] == -1)
				return -1;
			curBlock = FAT[curBlock];
			file.seekp(blocksBase + curBlock*BLOCK_SIZE + 1);
			nextBlock = blocksBase + curBlock*BLOCK_SIZE + BLOCK_SIZE;
		}

		else
			file.seekp((int)file.tellp() + (METADATA_SIZE - (MAX_NAME_SIZE + 1)));
		// Busca o próximo diretório
		if (file.get() == ' ')
			return -1;
		else {
			file.seekp((int)file.tellp() - 1);
			getline(file, currentDir, '|');
			currentDir = trim(currentDir);
		}
	}

	if (change) {
		file.seekp((int)file.tellp() + DIGITS_FILESIZE + TIMESTAMP_SIZE + 2); // 2 == separador
		if (change == 3) {
			changeTimeStamp(file); //Modificação
			changeTimeStamp(file); //Acesso
		}
		else if (change == 2) {
			changeTimeStamp(file); //Modificação
			file.seekp((int)file.tellp() + TIMESTAMP_SIZE + 1);
		}
		else if (change == 1) {
			file.seekp((int)file.tellp() + TIMESTAMP_SIZE + 1);
			changeTimeStamp(file); //Acesso
		}
	}
	else {
		file.seekp((int)file.tellp() + (METADATA_SIZE - MAX_NAME_SIZE - DIGITS_BLOCKADDRESS - 2)); // 2 == separador
	}
	
	string block;
	getline(file, block, '|');
	block = trim(block);
	return stoi(block);	
}