using namespace std;
#include <ed.hpp>

int searchFreeBlock(vector<bool>&bitmap) {
    int block = -1;
    for (int i = 0; i < (int)MAX_BLOCKS_SIZE; i++) {
		if (bitmap[i] == true) {
			bitmap[i] = false;
			block = i;
			break;
		}
	}
    return block;
}

void removeFileFromED(vector<bool>&bitmap, vector<int>&FAT, int block) {
	bitmap[block] = 1;
	while (FAT[block] != -1) {
		int aux = block;
		block = FAT[block];
		FAT[aux] = -1;
		bitmap[block] = 1;
	}
}

int filesystemSpaceRemaining(vector<bool>&bitmap) {
	int freeBlocks = 0;
	for (int i = 0; i< (int)bitmap.size(); i++) {
		if (bitmap[i] == true)
			freeBlocks++;
	}
	return BLOCK_SIZE*freeBlocks;
}

void unmountBITMAP(fstream & file, vector<bool>&bitmap) {
	for (int i = 0, j = 0; i < BLOCK_SIZE; i++, j++) {
			if (j < MAX_BLOCKS_SIZE) {
				file << bitmap[j];
				if (i+1 >= BLOCK_SIZE) 
					i = -1;
			}
			else {
				if (i+1 >= BLOCK_SIZE)
					file << "|";
				else
					file << " ";
			}
	}
}

void unmountFAT(fstream & file, vector<int>&FAT) {
	for (int i = 0; i < MAX_BLOCKS_SIZE; i++) {
		file << setfill(' ') << setw(5) << (to_string(FAT[i]));
	}

	// Resto do bloco
	for (int i = 0; i < BLOCK_SIZE - (5*MAX_BLOCKS_SIZE % BLOCK_SIZE) - 1; i++) {
		file << " ";
	}
	file << "|";
}