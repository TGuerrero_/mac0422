using namespace std;
#include <externals.hpp>
#include <config.hpp>
#include <file.hpp>
#include <ed.hpp>
#include <aux.hpp>


fstream fileSystem;
int blocksBase;
vector<bool> bitmap;
vector<int> FAT;

void rm(string arquivo);

/* Funções Auxiliares */

/*
* parse()
* Faz o parse de uma linha pelo separador "separator".
*/
vector<string> parse(string command, char separator) {
	size_t current, previous = 0;
	vector<string> parsed;

	current = command.find(separator);
	while (current != string::npos) {
		parsed.push_back(command.substr(previous, current - previous));
		previous = current + 1;
		current = command.find(separator, previous);
	}
	parsed.push_back(command.substr(previous, current - previous));

	if (separator == '/') {
		parsed[0] = '/';
	}
	return parsed;
}

/*
* rmdirR()
* Remove recursivamente todos os diretórios/arquivos abaixo de "block".
*/
void rmdirR(string path, int block) {
	fileSystem.seekp(blocksBase + block*BLOCK_SIZE + 1);

	string name, size, fileBlock;
	bool inTheSameDir = true;

	int nextBlock = blocksBase + block*BLOCK_SIZE + BLOCK_SIZE;
	while (inTheSameDir) {
		if (fileSystem.get() == ' ')
			inTheSameDir = false;
		else {
			fileSystem.seekg((int)fileSystem.tellg() - 1);
			getline(fileSystem, name, '|');
			name = trim(name);
			getline(fileSystem, size, '|');
			size = trim(size);

			fileSystem.seekg((int)fileSystem.tellg() + (3*TIMESTAMP_SIZE + 3));
			getline(fileSystem, fileBlock, '|');
			fileBlock = trim(fileBlock);
			
			int curPos = fileSystem.tellg();
			if (name != "@") {
				if (stoi(size) == -1) {
					rmdirR((path + '/' + name), stoi(fileBlock));
					cout << "Foi removido o diretório: " << (path + '/' + name) << endl;
				}
				else {
					removeFileFromED(bitmap, FAT, stoi(fileBlock));
					cout << "Removido: " << (path + '/' + name) << endl;
				}				
			}
			fileSystem.seekg(curPos);
			if ((int)fileSystem.tellg() >= nextBlock) {
				// Fim de bloco
				if (FAT[block] == -1)
					inTheSameDir = false;
				else {
					block = FAT[block];
					fileSystem.seekg(blocksBase + block*BLOCK_SIZE + 1);
					nextBlock = blocksBase + block*BLOCK_SIZE + BLOCK_SIZE;
				}
			}
		}
	}
	removeFileFromED(bitmap, FAT, block);
}

/*
* countFiles()
* Recebe um bloco e retorna um vetor contendo:
* vector[0] - Quantidade de diretórios existentes a partir de "block";
* vector[1] - Quantidade de arquivos existentes a patir de "block";
* vector[2] - Quantidade de espaço desperdiçado a partir de "block"(incluso).
*/
vector<int> countFiles(int block) {
	vector<int>data(3, 0);
	string name, size, fileBlock;
	bool inTheSameDir = true;
	int count = 0, nextBlock;

	if (block == 0) {
		count = 1;
		data[0] = 1;
	}

	fileSystem.seekg(blocksBase + block*BLOCK_SIZE + 1);
	nextBlock = blocksBase + block*BLOCK_SIZE + BLOCK_SIZE;
	while (inTheSameDir) {
		if (fileSystem.get() == ' ')
			inTheSameDir = false;
		else {
			fileSystem.seekg((int)fileSystem.tellg() - 1);
			getline(fileSystem, name, '|');
			name = trim(name);
			getline(fileSystem, size, '|');
			size = trim(size);

			fileSystem.seekg((int)fileSystem.tellg() + (3*TIMESTAMP_SIZE + 3));
			getline(fileSystem, fileBlock, '|');
			fileBlock = trim(fileBlock);

			int curPos = fileSystem.tellg();
			if (name != "@" && name != "/") {
				count++;
				if (stoi(size) == -1) {
					vector<int> aux = countFiles(stoi(fileBlock));
					data[0] += aux[0] + 1;
					data[1] += aux[1];
					data[2] += aux[2];
				}
				else {
					data[1]++;
					data[2] += BLOCK_SIZE - (stoi(size)%BLOCK_SIZE + 1);
				}
			}
			fileSystem.seekg(curPos);
			if ((int)fileSystem.tellg() >= nextBlock) {
				// Fim de bloco
				if (FAT[block] == -1)
					inTheSameDir = false;
				else {
					block = FAT[block];
					count = 0;
					fileSystem.seekg(blocksBase + block*BLOCK_SIZE + 1);
					nextBlock = blocksBase + block*BLOCK_SIZE + BLOCK_SIZE;
				}
			}
		}
	}
	data[2] += ((MAX_FILES_PER_BLOCK - count)*METADATA_SIZE)%BLOCK_SIZE;
	return data;
}

/* Funções Principais */

void mount(string arquivo) {
	bool isNewFile = !checkFile(arquivo);

	fileSystem.open(arquivo, fstream::in | fstream::out);
	if (fileSystem.fail()) {
		cout << "ERRO: Não foi possível abrir/criar o arquivo!\n" <<endl;
	}

	if (isNewFile) {
		//bitmap
		bitmap = vector<bool>(MAX_BLOCKS_SIZE, 1);
		FAT = vector<int>(MAX_BLOCKS_SIZE, -1);
		
		bitmap[0] = false; // Diretório "/"
		for (int i = 0, j = 0; i < BLOCK_SIZE; i++, j++) {
			if (j < MAX_BLOCKS_SIZE) {
				fileSystem << bitmap[j];
				if (i+1 >= BLOCK_SIZE) 
					i = -1;
			}
			else {
				if (i+1 >= BLOCK_SIZE)
					fileSystem << "|";
				else
					fileSystem << " ";
			}
		}
		
		//FAT
		for (int i = 0; i < MAX_BLOCKS_SIZE; i++) {
				fileSystem << "   -1";
		}

		// Resto do bloco
		for (int i = 0; i < BLOCK_SIZE - (5*MAX_BLOCKS_SIZE % BLOCK_SIZE) - 1; i++) {
			fileSystem << " ";
		}
		fileSystem << "|";

		blocksBase = fileSystem.tellp();
		fillBlock(fileSystem, blocksBase, 0);
		fileSystem << "?";
		putMetadata(fileSystem, "/", -1, 0);
	}
	else {
		bitmap = vector<bool>(MAX_BLOCKS_SIZE);
		FAT = vector<int>(MAX_BLOCKS_SIZE);
		char bit[6];

		//bitmap
		for (int i = 0; fileSystem.get(bit, 2) && bit[0] != '|'; i++) {
			if (i < MAX_BLOCKS_SIZE)
				bitmap[i] = atoi(bit);
		}

		//FAT
		for (int i = 0; i < MAX_BLOCKS_SIZE && fileSystem.get(bit, 6); i++) {
			FAT[i] = atoi(bit);
		}

		// Resto do bloco
		fileSystem.seekg((int)fileSystem.tellg() + BLOCK_SIZE - (5*MAX_BLOCKS_SIZE % BLOCK_SIZE));

		blocksBase = fileSystem.tellg();
	}
}

void cp(string origem, string destino) {
	vector<string> path = parse(destino, '/');
	string newFileName = path[path.size() - 1];
	fstream sourceFile(origem);
	int sourceFileSize, qtdBlocks;	

	if (sourceFile.fail()) {
		cout << "ERRO: Não foi possível abrir o arquivo!\n" <<endl;
		return;
	}

	if (newFileName.length() > MAX_NAME_SIZE) {
		cout << "ERRO: Nome muito grande!" << endl << "(Máximo: "<< MAX_NAME_SIZE << " caracteres)" << endl;
		return;
	}

	sourceFileSize = getFileSize(sourceFile);
	qtdBlocks = (sourceFileSize/(BLOCK_SIZE-1)) + 1;
	vector<int>storedBlocks(qtdBlocks, -1);

	// Faz a busca por blocos livres
	for (int i = 0; i < qtdBlocks; i++) {
		storedBlocks[i] = searchFreeBlock(bitmap);
		if (storedBlocks[i] == -1) {
			cout << "ERRO: Não foi possível criar o arquivo (sem espaço)" << endl;
			for (int j = 0; j < i; j++) {
				bitmap[storedBlocks[j]] = 1;
			}
			return;
		}
	}

	// Faz a busca pelo diretório pai
	fileSystem.seekg(blocksBase + 1);
	int fathersBlock = 0;
	for (size_t i = 0; i < path.size() - 1; i++) {
		if (i == path.size() - 2) {
			// Altera os tempos do diretório pai
			fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 3);	
		}
		else
			fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 0);
		if (fathersBlock == -1) {
			cout << "O diretorio pai não existe em " << destino << endl;
			return;
		}

		fileSystem.seekg(blocksBase + BLOCK_SIZE*fathersBlock + 1);
	}


	// Busca na FAT o último bloco do diretório pai
	while (FAT[fathersBlock] != -1) {
		fathersBlock = FAT[fathersBlock];
	}

	// Insere os metadados
	if (findSpaceForMetadata(fileSystem, FAT, bitmap, fathersBlock, blocksBase)) {
		cout << "ERRO: Não foi possível criar o arquivo (sem espaço)" << endl;
		for (int i = 0; i < qtdBlocks; i++) {
			bitmap[storedBlocks[i]] = 1;
		}
		return;
	}

	putMetadata(fileSystem, newFileName, sourceFileSize, storedBlocks[0]);

	// Aloca o bloco de memória
	fillBlock(fileSystem, blocksBase, storedBlocks[0]);
	fileSystem.seekp((int)fileSystem.tellp() + 1);

	char currentContent[BLOCK_SIZE];
	for (int i = 0; !sourceFile.eof(); i++) {
		sourceFile.read(currentContent, BLOCK_SIZE-1);
		fileSystem.write(currentContent, sourceFile.gcount());
		if (!sourceFile.eof()) {
			//Resize
			FAT[storedBlocks[i]] = storedBlocks[i+1];
			FAT[storedBlocks[i+1]] = -1;
			fillBlock(fileSystem, blocksBase, storedBlocks[i+1]);
			fileSystem.seekp((int)fileSystem.tellp() + 1);
		}
	}

	sourceFile.close();
}

void mkdir(string diretorio) {
	vector<string> path = parse(diretorio, '/');
	string newDirName = path[path.size() - 1];

	if (newDirName.length() > MAX_NAME_SIZE) {
		cout << "ERRO: Nome muito grande!" << endl << "(Máximo: "<< MAX_NAME_SIZE << " caracteres)" << endl;
		return;
	}

	// Faz a busca por um bloco livre
	int storedBlock = searchFreeBlock(bitmap);
	if (storedBlock == - 1) {
		cout << "ERRO: Não foi possível criar o diretório (sem espaço)" << endl;
		return;
	}

	// Faz a busca pelo diretório pai
	fileSystem.seekg(blocksBase + 1);
	int fathersBlock = 0;
	for (size_t i = 0; i < path.size() - 1; i++) {
		if (i == path.size() - 2) {
			// Altera os tempos do diretório pai
			fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 3);
		}
		else
			fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 0);
		if (fathersBlock == -1) {
			cout << "O diretorio pai não existe em " << diretorio << endl;
			return;
		}

		fileSystem.seekg(blocksBase + BLOCK_SIZE*fathersBlock + 1);
	}

	// Busca na FAT o último bloco do diretório pai
	while (FAT[fathersBlock] != -1) {
		fathersBlock = FAT[fathersBlock];
	}

	// Insere os metadados
	if (findSpaceForMetadata(fileSystem, FAT, bitmap, fathersBlock, blocksBase)) {
		cout << "ERRO: Não foi possível criar o diretório (sem espaço)" << endl;
		bitmap[storedBlock] = true;
		return;
	}
	putMetadata(fileSystem, newDirName, -1, storedBlock);

	// Aloca o bloco de memória
	fillBlock(fileSystem, blocksBase, storedBlock);
}

void rmdir(string diretorio) {
	vector<string> path = parse(diretorio, '/');
	string dirName = path[path.size() - 1];

	// Procura o diretorio pai
	fileSystem.seekg(blocksBase + 1);
	int dirBlock = 0;
	for (size_t i = 0; i < path.size() - 1; i++) {
		if (i == path.size() - 2) {
			// Altera os tempos do diretório pai
			dirBlock = searchFile(fileSystem, blocksBase, FAT, path[i], dirBlock, 3);
		}
		else
			dirBlock = searchFile(fileSystem, blocksBase, FAT, path[i], dirBlock, 0);
		if (dirBlock == -1) {
			cout << "O diretorio não existe em " << diretorio << endl;
			return;
		}

		fileSystem.seekg(blocksBase + BLOCK_SIZE*dirBlock + 1);
	}

	// Procura os metadados do arquivo a ser deletado
	string name;
	getline(fileSystem, name, '|');
	name = trim(name);

	while (name != dirName) {
		fileSystem.seekg((int)fileSystem.tellg() + (METADATA_SIZE - MAX_NAME_SIZE - 1));
		getline(fileSystem, name, '|');
		name = trim(name);
	}

	// Lazy deletion
	fileSystem.seekp((int)fileSystem.tellp() - (MAX_NAME_SIZE + 1));
	fileSystem << '@';
	for (int i = 0; i < (int)(MAX_NAME_SIZE - 1); i++)
		fileSystem << " ";
	fileSystem << '|';

	fileSystem.seekp((int)fileSystem.tellp() + (DIGITS_FILESIZE + 3*TIMESTAMP_SIZE + 4));
	string dirBlockS;
	getline(fileSystem, dirBlockS, '|');
	dirBlock = stoi(trim(dirBlockS));

	rmdirR(diretorio, dirBlock);

	removeFileFromED(bitmap, FAT, dirBlock);
	
	cout << "Foi removido o diretório: " << diretorio << endl;
}

void cat(string arquivo) {
	vector<string> path = parse(arquivo, '/');
	string fileName = path[path.size() - 1];	

	// Faz a busca pelo diretório pai
	fileSystem.seekg(blocksBase + 1);
	int fileBlock = 0;
	for (size_t i = 0; i < path.size() - 1; i++) {
		fileBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fileBlock, 0);
		fileSystem.seekg(blocksBase + BLOCK_SIZE*fileBlock + 1);
	}

	// Faz a busca pelos metadados do arquivo
	string name;
	getline(fileSystem, name, '|');
	name = trim(name);

	while (name != fileName) {
		fileSystem.seekg((int)fileSystem.tellg() + (METADATA_SIZE - MAX_NAME_SIZE - 1));
		getline(fileSystem, name, '|');
		name = trim(name);
	}

	string fileSizeS;
	getline(fileSystem, fileSizeS, '|');
	int fileSize = stoi(trim(fileSizeS));

	fileSystem.seekg((int)fileSystem.tellg() + 3*TIMESTAMP_SIZE + 3);
 
	string fileBlockS;
	getline(fileSystem, fileBlockS, '|');
	fileBlock = stoi(trim(fileBlockS));

	// Printa o conteúdo do arquivo
	fileSystem.seekg(blocksBase + fileBlock*BLOCK_SIZE + 1);
	while (fileSize > 0) {
		char currentContent[BLOCK_SIZE] = "";
		if (fileSize > BLOCK_SIZE - 1) {
			fileSystem.read(currentContent, BLOCK_SIZE-1);
			fileSize -= (BLOCK_SIZE - 1);
			fileBlock = FAT[fileBlock];
			fileSystem.seekg(blocksBase + fileBlock*BLOCK_SIZE + 1);
		}
		else {
			fileSystem.read(currentContent, fileSize);
			fileSize = 0;
		}
		cout << currentContent;
	}
	cout << endl;
}

void touch(string arquivo) {
	vector<string> path = parse(arquivo, '/');
	string newFileName = path[path.size() - 1];

	if (newFileName.length() > MAX_NAME_SIZE) {
		cout << "ERRO: Nome muito grande!" << endl << "(Máximo: "<< MAX_NAME_SIZE << " caracteres)" << endl;
		return;
	}

	// Faz a busca pelo diretório pai e pelo arquivo
	fileSystem.seekg(blocksBase + 1);
	int fathersBlock = 0, fileBlock, grandBlock;
	for (size_t i = 0; i < path.size(); i++) {
		if (i == path.size() - 1) {
			fileBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 3);
			if (fileBlock != -1) {
				// Arquivo já existe
				return;
			}
		}
		else {
			if (i == path.size() - 2) {
				// Altera os tempos do diretório pai
				grandBlock = fathersBlock;
				fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 2);
			}
			else
				fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fathersBlock, 0);
		}
		if (fathersBlock == -1) {
			cout << "O diretorio pai não existe em " << arquivo << endl;
			return;
		}
		fileSystem.seekg(blocksBase + BLOCK_SIZE*fathersBlock + 1);
	}
	// O arquivo não existe, ou seja, atualiza o timestamp de modificação
	fathersBlock = searchFile(fileSystem, blocksBase, FAT, path[path.size() - 2], grandBlock, 3);
	if (fathersBlock == -1) {
		cout << "O diretorio pai não existe em " << arquivo << endl;
		return;
	}

	// Faz a busca por um bloco livre
	int storedBlock = searchFreeBlock(bitmap);
	if (storedBlock == -1) {
		cout << "ERRO: Não foi possível criar o arquivo (sem espaço)" << endl;
		return;
	}

	// Busca na FAT o último bloco do diretório pai
	while (FAT[fathersBlock] != -1) {
		fathersBlock = FAT[fathersBlock];
	}

	// Insere os metadados
	if (findSpaceForMetadata(fileSystem, FAT, bitmap, fathersBlock, blocksBase)) {
		cout << "ERRO: Não foi possível criar o arquivo (sem espaço)" << endl;
		bitmap[storedBlock] = true;
		return;
	}
	putMetadata(fileSystem, newFileName, 0, storedBlock);

	// Aloca o bloco de memória
	fillBlock(fileSystem, blocksBase, storedBlock);
}

void rm(string arquivo) {
	vector<string> path = parse(arquivo, '/');
	string fileName = path[path.size() - 1];	

	// Procura o diretorio pai
	fileSystem.seekg(blocksBase + 1);
	int fileBlock = 0;
	for (size_t i = 0; i < path.size() - 1; i++) {
		if (i == path.size() - 2) {
			// Altera os tempos do diretório pai
			fileBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fileBlock, 3);
		}
		else
			fileBlock = searchFile(fileSystem, blocksBase, FAT, path[i], fileBlock, 0);
		if (fileBlock == -1) {
			cout << "Este arquivo não existe em " << arquivo << endl;
			return;
		}
		fileSystem.seekg(blocksBase + BLOCK_SIZE*fileBlock + 1);
	}

	// Procura os metadados do arquivo a ser deletado
	string name;
	getline(fileSystem, name, '|');
	name = trim(name);

	while (name != fileName) {
		fileSystem.seekg((int)fileSystem.tellg() + (METADATA_SIZE - MAX_NAME_SIZE - 1));
		getline(fileSystem, name, '|');
		name = trim(name);
	}

	// Lazy deletion
	fileSystem.seekp((int)fileSystem.tellp() - (MAX_NAME_SIZE + 1));
	fileSystem << '@';
	for (int i = 0; i < (int)(MAX_NAME_SIZE - 1); i++)
		fileSystem << " ";
	fileSystem << '|';

	fileSystem.seekp((int)fileSystem.tellp() + (DIGITS_FILESIZE + 3*TIMESTAMP_SIZE + 4));
	string fileBlockS;
	getline(fileSystem, fileBlockS, '|');
	fileBlock = stoi(trim(fileBlockS));

	removeFileFromED(bitmap, FAT, fileBlock);
}

void ls(string diretorio) {
	vector<string> targetDir;
	char current;

	if (diretorio != "/")
		targetDir = parse(diretorio, '/');

	// Faz a busca pelo arquivo
	fileSystem.seekg(blocksBase + 1);
	int dirBlock = 0;
	for (size_t i = 0; i < targetDir.size(); i++) {
		dirBlock = searchFile(fileSystem, blocksBase, FAT, targetDir[i], dirBlock, 0);
		if (dirBlock == -1) {
			cout << "Este diretorio não existe em " << diretorio << endl;
			return;
		}
		fileSystem.seekg(blocksBase + BLOCK_SIZE*dirBlock + 1);
	}

	while ((current = fileSystem.get()) != ' ' && current != EOF) {
		if (current == '?') {
			dirBlock = FAT[dirBlock];
			if (dirBlock != -1)
				fileSystem.seekg(blocksBase + dirBlock*BLOCK_SIZE + 1);
			else
				break;
		}
		else {
			string name, size, modTime;
			fileSystem.seekg((int)fileSystem.tellg() - 1);
			getline(fileSystem, name, '|');
			name = trim(name);
			getline(fileSystem, size, '|');
			size = trim(size);
			fileSystem.seekg((int)fileSystem.tellg() + TIMESTAMP_SIZE + 1);
			getline(fileSystem, modTime, '|');

			if (name != "/" && name != "@") {
				if (size == "-1") {
					cout << "\033[1;34m";
					cout << setfill(' ') << setw(7) << "DIR";
					cout << "    | " << modTime << " | " << name << endl;
					cout << "\033[1;0m";
				}
				else {
					int Ksize = stoi(size);
					printSize(Ksize);
					cout << " | " << modTime << " | " << name << endl;
				}
			}

			fileSystem.seekg((int)fileSystem.tellg() + TIMESTAMP_SIZE + DIGITS_BLOCKADDRESS + 2);
		}
	}
}

void find(string diretorio, string arquivo) {
	vector<string> local;
	string fileSize, fileBlock, fileName;
	queue<pair<string, int>> seenDirs;
	int nextBlock, localBlock;

	if (diretorio != "/")
		local = parse(diretorio, '/');

	// Faz a busca pelo diretório
	fileSystem.seekg(blocksBase + 1);
	localBlock = 0;
	for (size_t i = 0; i < local.size(); i++) {
		localBlock = searchFile(fileSystem, blocksBase, FAT, local[i], localBlock, 0);
		if (localBlock == -1) {
			cout << "O diretorio pai não existe em " << arquivo << endl;
			return;
		}

		fileSystem.seekg(blocksBase + BLOCK_SIZE*localBlock + 1);
	}
	
	// BFS printando as ocorrencias de 'arquivo' nos diretorios
	seenDirs.push({diretorio, localBlock});
	while (!seenDirs.empty()) {
		string curPath = seenDirs.front().first;
		int curBlock = seenDirs.front().second;
		seenDirs.pop();

		nextBlock = blocksBase + curBlock*BLOCK_SIZE + BLOCK_SIZE;
		fileSystem.seekg(blocksBase + curBlock*BLOCK_SIZE + 1);
		bool inTheSameDir = true;
		while (inTheSameDir) {
			if (fileSystem.get() == ' ')
				inTheSameDir = false;
			else {
				fileSystem.seekg((int)fileSystem.tellg() - 1);
				getline(fileSystem, fileName, '|');
				fileName = trim(fileName);
				getline(fileSystem, fileSize, '|');
				fileSize = trim(fileSize);

				if (stoi(fileSize) == -1 && fileName != "/") {
					// Diretório
					fileSystem.seekg((int)fileSystem.tellg() + 3*TIMESTAMP_SIZE + 3);
					getline(fileSystem, fileBlock, '|');
					fileBlock = trim(fileBlock);
					if (curPath == "/")
						seenDirs.push({'/' + fileName, stoi(fileBlock)});
					else
						seenDirs.push({curPath + '/' + fileName, stoi(fileBlock)});
				}
				else if (fileName == arquivo) {
					if (curPath == "/")
						cout << ('/' + fileName) << endl;
					else 
						cout << (curPath + '/' + fileName) << endl;

					fileSystem.seekg((int)fileSystem.tellg() + (METADATA_SIZE - MAX_NAME_SIZE - DIGITS_FILESIZE - 2));
				}
				else {
					fileSystem.seekg((int)fileSystem.tellg() + (METADATA_SIZE - MAX_NAME_SIZE - DIGITS_FILESIZE - 2));
				}
			}

			if ((int)fileSystem.tellg() >= nextBlock) {
				// Fim de bloco
				if (FAT[curBlock] == -1)
					inTheSameDir = false;
				else {
					curBlock = FAT[curBlock];
					fileSystem.seekg(blocksBase + curBlock*BLOCK_SIZE + 1);
					nextBlock = blocksBase + curBlock*BLOCK_SIZE + BLOCK_SIZE;
				}
			}
		}
	}
}

void df() {
	vector<int>data = countFiles(0);
	cout << "Diretórios: " << data[0] << endl;
	cout << "Arquivos: " << data[1] << endl;
	cout << "Espaço Livre: ";
	printSize(filesystemSpaceRemaining(bitmap));
	cout << endl;
	cout << "Espaço Desperdiçado: ";
	printSize(data[2]);
	cout << endl;
}

void unmount() {
	fileSystem.seekp(0);
	unmountBITMAP(fileSystem, bitmap);
	unmountFAT(fileSystem, FAT);
	fileSystem.close();
}

bool execute(vector<string> command) {
	if (command.size() > 1 && command[0] == "mount") 
		mount(command[1]);
	
	else if (command.size() > 2 && command[0] == "cp") 
		cp(command[1], command[2]);
	
	else if (command.size() > 1 && command[0] == "mkdir")
		mkdir(command[1]);
	
	else if (command.size() > 1 && command[0] == "rmdir") 
		rmdir(command[1]);
	
	else if (command.size() > 1 && command[0] == "cat") 
		cat(command[1]);	
	
	else if (command.size() > 1 && command[0] == "touch") 
		touch(command[1]);
	
	else if (command.size() > 1 && command[0] == "rm") 
		rm(command[1]);
	
	else if (command.size() > 1 && command[0] == "ls") 
		ls(command[1]);
	
	else if (command.size() > 2 && command[0] == "find") 
		find(command[1], command[2]);
	
	else if (command[0] == "df") 
		df();
	
	else if (command[0] == "unmount") 
		unmount();
	
	else if (command[0] == "sai")
		return false;

	else
		cout << "Comando inválido" << endl;
	
	return true;
} 

int main() {
	bool prompt = true;
	string command;
	vector<string> parsedCommand;

	while (prompt) {
		cout <<"\033[1;32m";
		cout << "[EP3]: ";
		cout <<"\033[1;0m";
		getline(cin, command);
		parsedCommand = parse(command, ' ');
		prompt = execute(parsedCommand);
	}

	return 0;
}