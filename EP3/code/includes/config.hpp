using namespace std;
#ifndef CONFIG_HPP
#define CONFIG_HPP
#include <string>

const int BLOCK_SIZE = 4000;
const int MAX_SIZE = (100e6);

/*
BITMAP: 1 byte por bloco => 25.000 bytes => 7 blocos
FAT: 5 bytes por bloco => 125.000 => 32 blocos
*/
const int MAX_BLOCKS_SIZE = (MAX_SIZE/BLOCK_SIZE) - 39;

const int METADATA_SIZE = 129; // Com separadores
const int MAX_FILES_PER_BLOCK = (BLOCK_SIZE - 1)/METADATA_SIZE; //-1 == ?

// Tamanho metadados
const int MAX_NAME_SIZE = 37;
const int DIGITS_FILESIZE = 9; // 100MB
const int TIMESTAMP_SIZE = 24;
const int DIGITS_BLOCKADDRESS = (int)to_string(MAX_BLOCKS_SIZE).length();
#endif