using namespace std;
#ifndef FILE_HPP
#define FILE_HPP
#include <externals.hpp>
#include <config.hpp>
#include <aux.hpp>
#include <ed.hpp>

/*
* fillBlock()
* Aloca o espaço do bloco "block" com conteúdo vazio
* No final faz o ponteiro do arquivo apontar pro início do bloco
*/
void fillBlock(fstream & file, int blocksBase, int block);

/*
* findSpaceForMetadata()
* Procura a partir do bloco "block" qual é a próxima posição livre para a inserção
* de metadados. No final da função a cabeça de leitura do "file" estará apontando para a posição livre.
* Retorna true se não conseguir achar um espaço livre e false caso contrário.
*/
bool findSpaceForMetadata(fstream & file, vector<int>&FAT, vector<bool>&bitmap, int block, int blocksBase);

/*
* putMetadata()
* Insere os metadados de um arquivo/diretório no lugar
* onde está apontando a cabeça de escrita do arquivo
*/
void putMetadata(fstream & file, string name, int size, int block);


/*
* changeTimeStamp()
* Altera uma informação de tempo de um arquivo
*/
void changeTimeStamp(fstream & file);

/*
* searchFile()
* Recebe o nome de um arquivo/diretório, o bloco atual e uma flag "change".
* Muda a cabeça de leitura para o início do bloco "curBlock" no início da execução.
* change:
*   0 - Não modifica o tempo
*   1 - Modifica o tempo de modificação do "fileName" ao encontrá-lo
*   2 - Modifica o tempo de acesso do "fileName" ao encontrá-lo
*   3 - Modifica o tempo de modificação e acesso do "fileName" ao encontrá-lo
* Retorna o bloco que está os dados do diretório "fileName" ou -1 se não conseguir achar.
*/
int searchFile(fstream & file, int blocksBase, vector<int>&FAT, string fileName, int curBlock, int change);

#endif