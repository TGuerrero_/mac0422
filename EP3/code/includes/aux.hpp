using namespace std;
#ifndef AUX_HPP
#define AUX_HPP
#include <externals.hpp>

/*
* checkFile()
* Retorna True se o arquivo existe ou cria o arquivo e retorna False caso contrário.
*/
bool checkFile(string fileName);

/*
* getTimeStamp()
* Retorna uma string com as informações de data e hora atual
*/
string getTimeStamp();

/*
* trim()
* Remove espaços do fim da string
*/
string trim(string rawString);

/*
* getFileSize()
* Retorna o tamanho do arquivo apontado por file
*/
int getFileSize(fstream& file);

/*
* printSize()
* Faz um print formatado do tamanho de um arquivo
*/
void printSize(int size);
#endif