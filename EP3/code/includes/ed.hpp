using namespace std;
#ifndef ED_HPP
#define ED_HPP
#include <externals.hpp>
#include <config.hpp>

/*
* searchFreeBlock()
* Recebe o vetor de bitmap e procura um bloco livre
* Seta o bloco como ocupado e retorna o seu número ou somente retorna -1 se não foi possível achar
*/
int searchFreeBlock(vector<bool>&bitmap);

/*
* removeFileFromED()
* Remove da FAT e do bitmap todos os blocos alocados pelo
* arquivo que começa no bloco "block"
*/
void removeFileFromED(vector<bool>&bitmap, vector<int>&FAT, int block);


/*
* filesystemSpaceRemaining
* Retorna o espaço livre(em bytes) do sistema de arquivo;
*/
int filesystemSpaceRemaining(vector<bool>&bitmap);

/*
* unmountBITMAP()
* Escreve o conteúdo do vetor "bitmap" no arquivo file
*/
void unmountBITMAP(fstream & file, vector<bool>&bitmap);

/*
* unmountBITMAP()
* Escreve o conteúdo do vetor "FAT" no arquivo file
*/
void unmountFAT(fstream & file, vector<int>&FAT);

#endif