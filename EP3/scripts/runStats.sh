#!/bin/bash

g++ -o ./scripts/statsGenerator ./scripts/statsGenerator.cpp

mkdir -p ./results/statistics

for i in env0 env10 env50; do
	for j in {1..8}; do
		mkdir -p ./results/statistics/${i}
		./scripts/statsGenerator ./results/${i}/${j} > ./results/statistics/${i}/result${j}.txt
	done
done