#!/bin/bash

cd ./RealFileSystem

rm -f file1MB.txt
touch file1MB.txt

yes a | head -c 1MB | tr "\n" " " > file1MB.txt

rm -f file10MB.txt
touch file10MB.txt

yes a | head -c 10MB | tr "\n" " " > file10MB.txt

rm -f file30MB.txt
touch file30MB.txt

yes a | head -c 30MB | tr "\n" " " > file30MB.txt

rm -f file50MB.txt
touch file50MB.txt

yes a | head -c 50MB | tr "\n" " " > file50MB.txt

rm -f dummy.txt
touch dummy.txt

yes a | head -c 10 | tr "\n" " " > dummy.txt
