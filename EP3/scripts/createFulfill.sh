#!/bin/bash

rm -f ./scripts/fulfill.txt
touch ./scripts/fulfill.txt
echo "mount teste2.txt" >> ./scripts/fulfill.txt

# Coloque aqui o número de arquivos (930 <= MAX <= 25561)
MAX=24960

for i in {1..30}; do
    echo "mkdir /teste$i" >> ./scripts/fulfill.txt
done

for i in {1..30}; do
    for j in {1..30}; do
        echo "mkdir /teste$i/teste$j" >> ./scripts/fulfill.txt
    done
done

count=930
p1=1
p2=1
p3=1
while [[ ${count} -lt ${MAX} ]]; do
    echo "mkdir /teste${p1}/teste${p2}/teste${p3}" >> ./scripts/fulfill.txt
    if [[ p2 -eq 30 ]];
    then
        p2=1
        p1=$((${p1} + 1))
    fi
    if [[ p3 -eq 30 ]];
    then
        p3=0
        p2=$((${p2} + 1))
    fi
    p3=$((${p3} + 1))
    count=$((${count} + 1))
done

echo "unmount" >> ./scripts/fulfill.txt
echo "sai" >> ./scripts/fulfill.txt
