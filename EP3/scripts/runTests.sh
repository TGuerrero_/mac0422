#!/bin/bash

#make clean
make

bash ./scripts/auxScripts.sh

mkdir -p results/backupFS

for env in 0 10 50; do
	rm -f fs.txt
	rm -f results/backupFS/envFile.txt
	touch results/backupFS/envFile.txt
	if [[ ${env} -gt 0 ]]
	then
		./ep3 < ./testFiles/create${env}MBfilledFS.txt
	else
		./ep3 < ./testFiles/createMtFS.txt
	fi
	cp fs.txt ./results/backupFS/envFile.txt

	for i in {1..8}; do
		mkdir -p ./results/env${env}/${i}
	done

	# Teste 1
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/envFile.txt ./fs.txt
		echo Teste: 1 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/create1MBfilledFS.txt ; } 2> ./results/env${env}/1/TIME${i}.txt 
	done

	# Teste 2
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/envFile.txt ./fs.txt
		echo Teste: 2 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/create10MBfilledFS.txt ; } 2> ./results/env${env}/2/TIME${i}.txt 
	done

	# Teste 3
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/envFile.txt ./fs.txt
		echo Teste: 3 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/create30MBfilledFS.txt ; } 2> ./results/env${env}/3/TIME${i}.txt 
	done

	rm -f fs.txt
	cp ./results/backupFS/envFile.txt ./fs.txt
	./ep3 < ./testFiles/create1MBfilledFS.txt

	rm -f results/backupFS/rm1MB.txt
	touch results/backupFS/rm1MB.txt
	cp fs.txt ./results/backupFS/rm1MB.txt

	# Teste 4
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/rm1MB.txt ./fs.txt
		echo Teste: 4 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/remove1MBfilledFS.txt ; } 2> ./results/env${env}/4/TIME${i}.txt 
	done

	rm -f fs.txt
	cp ./results/backupFS/envFile.txt ./fs.txt
	./ep3 < ./testFiles/create10MBfilledFS.txt

	rm -f results/backupFS/rm10MB.txt
	touch results/backupFS/rm10MB.txt
	cp fs.txt ./results/backupFS/rm10MB.txt

	# Teste 5
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/rm10MB.txt ./fs.txt
		echo Teste: 5 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/remove10MBfilledFS.txt ; } 2> ./results/env${env}/5/TIME${i}.txt 
	done

	rm -f fs.txt
	cp ./results/backupFS/envFile.txt ./fs.txt
	./ep3 < ./testFiles/create30MBfilledFS.txt

	rm -f results/backupFS/rm30MB.txt
	touch results/backupFS/rm30MB.txt
	cp fs.txt ./results/backupFS/rm30MB.txt

	# Teste 6
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/rm30MB.txt ./fs.txt
		echo Teste: 6 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/remove30MBfilledFS.txt ; } 2> ./results/env${env}/6/TIME${i}.txt
	done

	rm -f fs.txt
	cp ./results/backupFS/envFile.txt ./fs.txt
	./ep3 < ./testFiles/create30HeightMt.txt

	rm -f results/backupFS/30HeightMt.txt
	touch results/backupFS/30HeightMt.txt
	cp fs.txt ./results/backupFS/30HeightMt.txt

	# Teste 7
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/30HeightMt.txt ./fs.txt
		echo Teste: 7 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/remove30Height.txt ; } 2> ./results/env${env}/7/TIME${i}.txt 
	done
	
	rm -f fs.txt
	cp ./results/backupFS/envFile.txt ./fs.txt
	./ep3 < ./testFiles/create30HeightFilled.txt

	rm -f results/backupFS/30HeightFilled.txt
	touch results/backupFS/30HeightFilled.txt
	cp fs.txt ./results/backupFS/30HeightFilled.txt
	
	# Teste 8
	for i in {1..30}; do
		rm -f fs.txt
		cp ./results/backupFS/30HeightFilled.txt ./fs.txt
		echo Teste: 8 Env: ${env} Tentativa: $i
		{ time ./ep3 < ./testFiles/remove30Height.txt ; } 2> ./results/env${env}/8/TIME${i}.txt 
	done
done

