#!/bin/bash

cd ./results/statistics

for dir in env0 env10 env50; do
	echo -----------
	for file in {1..8}; do
		echo ""
		echo ------ENV: ${dir} Teste ${file}
		cat ./${dir}/result${file}.txt
	done
done