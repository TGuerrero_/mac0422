#include <bits/stdc++.h>

using namespace std;

#define NUM_MEASURES 30

int main (int argc, char * argv[]) {
	fstream logTime;

	string folder, timeS, fileTime;
	char buffer[15];
	double timeSum = 0;

	folder = argv[1];

	vector<double> time (NUM_MEASURES+1);
	
	for (int i = 1; i <= 30; i++) {
		logTime.open(folder + "/TIME" + to_string(i) + ".txt");

		logTime.get();
		logTime.get(buffer, 14);
		timeS = buffer;

		timeS[8] = '.';

		time[i] = stof(timeS.substr(7, 5));
		timeSum += time[i];

		logTime.close();
	}

	double average = timeSum/NUM_MEASURES;

	// variance estimator: s = sqrt(1 / (n - 1) * sum_1^n(X[i] - mean))
	double varianceEstimator = 0;
	for (int i = 1; i <= NUM_MEASURES; i++)
		varianceEstimator += (time[i] - average)*(time[i] - average);

	varianceEstimator *= (1/((double)NUM_MEASURES-1));

	varianceEstimator = sqrt(varianceEstimator);

	double ladoEsquerdo = average - 2.042*varianceEstimator;
	double ladoDireito = average + 2.042*varianceEstimator;

	cout.precision(3);
	cout << "Average time spent in ms: " << (average * 1000) << " ms"<< endl;

	cout << "Confidence interval time in ms: [" << (ladoEsquerdo * 1000) << " : " << (ladoDireito * 1000) <<  "]" << endl;
} 