#!/bin/bash

bash scripts/createFiles.sh

mkdir -p testFiles
cd testFiles

for names in createMtFS.txt create1MBfilledFS.txt create10MBfilledFS.txt add10MBtoFS.txt create30MBfilledFS.txt create50MBfilledFS.txt remove1MBfilledFS.txt remove10MBfilledFS.txt remove30MBfilledFS.txt create30HeightFilled.txt remove30Height.txt create30HeightMt.txt; do
	rm -f ${names}
	touch ${names}
	echo "mount fs.txt" >> ${names}
done

# --------

echo "cp file1MB.txt /file1MB.txt" >> create1MBfilledFS.txt

# --------

echo "cp file10MB.txt /file10MB.txt" >> create10MBfilledFS.txt

# --------

echo "cp file10MB.txt /file10MBA.txt" >> add10MBtoFS.txt

# --------

echo "cp file30MB.txt /file30MB.txt" >> create30MBfilledFS.txt

# --------

echo "cp file50MB.txt /file50MB.txt" >> create50MBfilledFS.txt

# --------

echo "rm /file1MB.txt" >> remove1MBfilledFS.txt

# --------

echo "rm /file10MB.txt" >> remove10MBfilledFS.txt

# --------

echo "rm /file30MB.txt" >> remove30MBfilledFS.txt

# --------

PATH="/fatherOfAll"

echo "mkdir /fatherOfAll" >> create30HeightMt.txt
for i in {1..30}; do
	echo "mkdir ${PATH}/level${i}" >> create30HeightMt.txt
	PATH=${PATH}/level${i}
done

# --------

PATH="/fatherOfAll"

echo "mkdir /fatherOfAll" >> create30HeightFilled.txt
for i in {1..30}; do
	echo "mkdir ${PATH}/level${i}" >> create30HeightFilled.txt
	if [[ $i -lt 11 ]]
	then
		for j in {1..20}; do
			echo "cp dummy.txt ${PATH}/level${i}/dummy${j}.txt" >> create30HeightFilled.txt
		done
	fi
	PATH=${PATH}/level${i}
done

# --------

echo "rmdir /fatherOfAll" >> remove30Height.txt


for names in createMtFS.txt create1MBfilledFS.txt create10MBfilledFS.txt add10MBtoFS.txt create30MBfilledFS.txt create50MBfilledFS.txt remove1MBfilledFS.txt remove10MBfilledFS.txt remove30MBfilledFS.txt create30HeightFilled.txt remove30Height.txt create30HeightMt.txt; do
	echo "unmount" >> ${names}
	echo "sai" >> ${names}
done