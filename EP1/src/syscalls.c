#include <includes.h>
#include <syscalls.h>

void run_mkdir(char ** parsed) {
	if (mkdir(parsed[1], 0777) == ERROR) {
		printf("\nmkdir: Não foi possível criar o diretório \"%s\"\n", parsed[1]);
	}
}

void run_kill(char ** parsed) {
	pid_t pid = atoi(parsed[2]);
	int sig = atoi(&parsed[1][1]); //parsed[1] == "-9" parsed[1][1] == "9"

	if (kill(pid, sig) == ERROR) {
		printf("kill: Erro ao terminar o processo \"%d\"\n", pid);
	}
}

void run_ln(char ** parsed) {
	char *target = parsed[2], *linkname = parsed[3];
	if (!strcmp(parsed[1], "-s") && symlink(target, linkname) == ERROR) {
		printf("ln: Erro ao criar link simbólico ao arquivo \"%s\"\n", target);
	}
}

void run_cd(char ** parsed) {
	char* path = parsed[1];
	if (chdir(path) == ERROR) {
		printf("cd: Erro ao tentar mudar para o diretório %s\n", path);
	}
}