#include <includes.h>
#include <queue.h>
#include <process.h>
#include <heap.h>

#define MAX_FILENAME_SIZE 40

/* Constante associada ao tempo de execução do Round-Robin */
const int QUANTUM = 1;

/* Path dos arquivos de entrada e saída */
char filename_input_prefix[MAX_FILENAME_SIZE] = "./inputs/";
char filename_output_prefix[MAX_FILENAME_SIZE] = "./outputs/";

typedef struct timeval Clock;

/* Global */
pthread_mutex_t mutex;
sig_atomic_t thread_complete = 0;
int flag_d = 0;

/*
* Seta a flag thread_complete como true
*/
void set_thread_complete(void *_){
	thread_complete = 1;
}

/*
* Thread do escalonador FCFS
*/
void *threadFCFS(void *process) {
	clock_t init = clock()/CLOCKS_PER_SEC;
	Process * cur_process = process;
	int opr = 1;
	if (flag_d){
		fprintf(stderr, "O processo \"%s\" começou a rodar na CPU %d\n", cur_process->name, sched_getcpu());
	}

	while(((clock()/CLOCKS_PER_SEC) - init) < cur_process->dt){
		opr = (opr/opr) + opr;
	}

	if (flag_d){
		fprintf(stderr, "Thread do processo \"%s\" foi finalizada com sucesso e liberou a CPU %d\n", cur_process->name, sched_getcpu());
	}
	return NULL;
}

/*
* Thread do escalonador SRTN
*/
void *threadSRTN(void *process){
	pthread_cleanup_push(set_thread_complete, NULL);
	Process *cur_process = process;
	int opr = 1;
	if (flag_d){
		fprintf(stderr, "O processo \"%s\" começou a rodar na CPU %d\n", cur_process->name, sched_getcpu());
	}

	while(cur_process->dt > 0){
		pthread_mutex_lock(&cur_process->thread_mutex);
		if (flag_d){
			fprintf(stderr, "O processo \"%s\" está usando a CPU %d\n", cur_process->name, sched_getcpu());
		}
		cur_process->dt--;
		pthread_mutex_unlock(&cur_process->thread_mutex);
		opr = (opr/opr) + opr;
		sleep(1);
	}
	
	if (flag_d){
		fprintf(stderr, "Thread do processo \"%s\" foi finalizada com sucesso e liberou a CPU %d\n", cur_process->name, sched_getcpu());
	}
	pthread_cleanup_pop(1);
	pthread_exit(NULL);
}

/*
 * Thread do escalonador Round Robin
 */
void * threadRR (void * process) {
	pthread_cleanup_push(set_thread_complete, NULL);

	Process * cur_process = process;
	int opr = 1;
	if (flag_d){
		fprintf(stderr, "O processo \"%s\" começou a rodar na CPU %d\n", cur_process->name, sched_getcpu());
	}

	while (cur_process->dt > 0){
		pthread_mutex_lock(&cur_process->thread_mutex);
		if (flag_d){
			fprintf(stderr, "O processo \"%s\" está usando a CPU %d\n", cur_process->name, sched_getcpu());
		}
		opr = (opr/opr) + opr;
		cur_process->dt--;
		pthread_mutex_unlock(&cur_process->thread_mutex);
		if (cur_process->dt == 0)
			pthread_mutex_lock(&mutex);
		sleep(1);
	}
	
	if (flag_d){
		fprintf(stderr, "Thread do processo \"%s\" foi finalizada com sucesso e liberou a CPU %d\n", cur_process->name, sched_getcpu());
	}
	pthread_cleanup_pop(1);
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
}

/*
* Recebe o tempo de início do programa.
* Retorna o tempo atual contado a patir do tempo de início do programa.
*/
int get_cur_time(Clock start) {
	Clock cur_time;
	gettimeofday(&cur_time, NULL);
	cur_time.tv_sec -= start.tv_sec;
	return cur_time.tv_sec;
}



int main (int argc, char *argv[]) {
	if (argc < 4) {
		printf("Erro: Quantidade de parâmetros inválida.\n");
		return(1);
	}
	/*Params: 
	Tipo de escalonador: 1 - FCFS, 2 - SRTN, 3 - Round-Robin
	Nome do arquivo input
	Nome do arquivo de output
	Flag "d" 
	*/
	int scheduler_type = atoi(argv[1]);
	char * filename_input = argv[2];
	char * filename_output = argv[3];
	FILE * input;
	FILE * output;
	Process *scheduler[MAX_PROCESSES];
	int scheduler_size = 0;
	int context_changes = 0;
	
	if (argc > 4 && !strcmp(argv[4], "d"))
		flag_d = 1;

	printf("# Bem vindo ao gerenciador de processos #\n");

	input = fopen(strcat(filename_input_prefix, filename_input), "r");
	output = fopen(strcat(filename_output_prefix, filename_output), "w");

	if (input == NULL) {
		printf(" >:( Arquivo não encontrado!\n" 
			"(arquivos de input devem ser colocados na pasta \"inputs\" no diretorio raiz!)\n");
		return 1;
	}
	if (output == NULL) {
		printf(" >:( Ocorreu um erro ao criar o arquivo de saída!\n" 
			"(arquivos de log serão colocados na pasta \"outputs\" no diretorio raiz!)\n");
		return 1;
	}

	//Warming up
	char name[MAX_PROCESS_NAME_SIZE];
	int t0;
	int dt;
	int deadline;
	while(fscanf(input, "%s %d %d %d", name, &(t0), &(dt), &(deadline)) != EOF){
		Process *new = malloc(sizeof(Process));
		strcpy(new->name, name);
		new->t0 = t0;
		new->dt = dt;
		new->deadline = deadline;
		if (scheduler_type != 1){
			//Inicializa os semáforos
			pthread_mutex_init(&new->thread_mutex, NULL);
			pthread_mutex_lock(&new->thread_mutex);
		}
		new->finalized = 0;
		scheduler[scheduler_size] = new;
		scheduler_size++;
	}

	//Scheduler
	Clock start;
	gettimeofday(&start, NULL);
	int cur_time;

	if (scheduler_type == 1){
		//First-Come First-Served(FCFS)
		printf("\nEscalonador escolhido: FCFS\n\n");
		pthread_t tid;
		for (int i=0; i < scheduler_size; i++){
			pthread_t tid;
			cur_time = get_cur_time(start);
			if (i != 0 && cur_time >= scheduler[i]->t0){
				//Mudança de contexto
				context_changes++;
			}
			while(cur_time < scheduler[i]->t0){
				sleep(1);
				cur_time = get_cur_time(start);
			}
			if (flag_d){
				fprintf(stderr, "Novo processo! Nome: %s |t0: %d |dt: %d | deadline: %d\n",
				scheduler[i]->name, scheduler[i]->t0, scheduler[i]->dt, scheduler[i]->deadline);
			}
			pthread_create(&tid, NULL, threadFCFS, scheduler[i]);
			pthread_join(tid, NULL);
			cur_time = get_cur_time(start);
			fprintf(output, "%s %d %d\n", scheduler[i]->name, cur_time, cur_time-scheduler[i]->t0);
			if (flag_d){
				fprintf(stderr, "Processo finalizado com sucesso! Linha de output: %s %d %d\n", scheduler[i]->name, cur_time, cur_time-scheduler[i]->t0);
			}
			free(scheduler[i]);
		}
	}
	else if(scheduler_type == 2){
		//Shortest Remaining Time Next(SRTN)
		printf("\nEscalonador escolhido: SRTN\n\n");
		Process *srtn_scheduler[MAX_PROCESSES];
		int srtn_scheduler_size = 0;
		int next_process = 0;
		int processes_ran = 0;

		//Inicialização min heap
		for (int i=0; i < scheduler_size; i++){
			srtn_scheduler[i] = NULL;
		}

		while (processes_ran < scheduler_size){
			cur_time = get_cur_time(start);
			if (!thread_complete && next_process < scheduler_size && scheduler[next_process]->t0 <= cur_time){
				//Existem novos processos
				Process *first = srtn_scheduler[0];
				//Seção crítica
				if (srtn_scheduler_size != 0)
					pthread_mutex_lock(&srtn_scheduler[0]->thread_mutex);
				while (next_process < scheduler_size && scheduler[next_process]->t0 <= cur_time){
					if (flag_d){
						fprintf(stderr, "Novo processo! Nome: %s |t0: %d |dt: %d | deadline: %d\n",
						scheduler[next_process]->name, scheduler[next_process]->t0, scheduler[next_process]->dt, scheduler[next_process]->deadline);
					}
					pthread_create(&scheduler[next_process]->tid, NULL, threadSRTN, scheduler[next_process]);
					srtn_scheduler[srtn_scheduler_size] = scheduler[next_process];
					srtn_scheduler_size++;
					next_process++;

					build_heap(srtn_scheduler, srtn_scheduler_size);
				}
				if (first != NULL && strcmp(first->name, srtn_scheduler[0]->name)){
					//Mudança de contexto
					context_changes++;
				}
				//Fim da seção crítica
				pthread_mutex_unlock(&srtn_scheduler[0]->thread_mutex);
			}

			if (thread_complete){
				//Thread terminou sua execução
				Process *ran = srtn_scheduler[0];
				srtn_scheduler_size--;
				thread_complete = 0;
				processes_ran++;
				pthread_mutex_destroy(&ran->thread_mutex);
				srtn_scheduler[0] = NULL;

				if (srtn_scheduler_size != 0){
					srtn_scheduler[0] = srtn_scheduler[srtn_scheduler_size];
					srtn_scheduler[srtn_scheduler_size] = NULL; //Reset no slot do vetor

					build_heap(srtn_scheduler, srtn_scheduler_size);
					context_changes++;
					pthread_mutex_unlock(&srtn_scheduler[0]->thread_mutex);
				}
				cur_time = get_cur_time(start);
				fprintf(output, "%s %d %d\n", ran->name, cur_time, cur_time-ran->t0);
				if (flag_d){
				fprintf(stderr, "Processo finalizado com sucesso! Linha de output: %s %d %d\n", ran->name, cur_time, cur_time-ran->t0);
				}
				free(ran);
			}
			usleep(0.3);
		}
	}
	else if (scheduler_type == 3) {
	 	//Round-Robin
	 	printf("\nEscalonador escolhido: Round-Robin\n\n");
	 	// Lista ligada circular que sempre retiramos o processo atual
	 	thread_cell * threads = NULL;
	 	
	 	int cur_process = 0;
	 	int processes_ran = 0;
	 	int init = get_cur_time(start);
	 	int notPreempted = 0;

		while (processes_ran < scheduler_size) {
			cur_time = get_cur_time(start);

			thread_cell * running_process = threads;

			if (threads == NULL || (cur_time - init) >= QUANTUM || thread_complete || notPreempted) {
 				/* Se temos algum processo rodando, paramos*/
 				if (running_process != NULL) {
 					if (!notPreempted)
 						pthread_mutex_lock(&running_process->process->thread_mutex);
 					pthread_mutex_lock(&mutex);
 					pthread_mutex_unlock(&mutex);
 				}

				if (cur_process < scheduler_size && cur_time >= scheduler[cur_process]->t0) {
					/* Recebemos todos os processos daquele instante */
	 				while (cur_process < scheduler_size && cur_time >= scheduler[cur_process]->t0) {
	 					pthread_create(&(scheduler[cur_process]->tid), NULL, threadRR, scheduler[cur_process]);
						if (flag_d){
							fprintf(stderr, "Novo processo! Nome: %s |t0: %d |dt: %d | deadline: %d\n",
							scheduler[cur_process]->name, scheduler[cur_process]->t0, scheduler[cur_process]->dt, scheduler[cur_process]->deadline);
						}
	 					threads = add_thread(threads, scheduler[cur_process]);
						cur_process++;
	 				}
				}
				// Thread Complete
				if (thread_complete) {
					fprintf(output, "%s %d %d\n", threads->process->name, cur_time, cur_time - threads->process->t0);
					if (flag_d){
						fprintf(stderr, "Processo finalizado com sucesso! Linha de output: %s %d %d\n", threads->process->name, cur_time, cur_time - threads->process->t0);
					}
					pthread_mutex_destroy(&threads->process->thread_mutex);
					threads = remove_thread(threads);
					thread_complete = 0;
					processes_ran++;
					notPreempted = 1;
				}
				else if (running_process != NULL) {
					if (notPreempted)
						notPreempted = 0;
					else
						threads = threads->next;
				}
				if (!notPreempted && threads != NULL) {
					if (running_process != NULL){
						context_changes++;
					}
					init = get_cur_time(start);
					pthread_mutex_unlock(&threads->process->thread_mutex);
				}
			}
			usleep(0.05);
		}
	}
	if (flag_d){
		fprintf(stderr, "Quantidade de mudanças de contexto: %d\n", context_changes);
	}
	fprintf(output, "%d\n", context_changes);
	fclose(input);
	fclose(output);
	return 0;
}
