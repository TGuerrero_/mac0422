#include <includes.h>
#include <queue.h>

/* A lista ligada implementada é circular e sempre retiramos do início pois somente o processo atual pode
 * ter acabado.
 */

thread_cell * add_thread(thread_cell * ini, Process * process_thread) {
	thread_cell * new_thread = malloc(sizeof(thread_cell));
	new_thread->process = process_thread;

	if (ini == NULL) {
		ini = new_thread;
		ini->next = ini;
	}
	else {
		thread_cell * aux = ini;
		while (aux->next != ini)
			aux = aux->next;
		new_thread->next = ini;
		aux->next = new_thread;
	}
	return ini;
}

thread_cell * remove_thread(thread_cell * ini) {
	if (ini == NULL)
		return NULL;

	if (ini->next == ini) {
		free(ini);
		ini = NULL;
	}
	else {
		thread_cell * aux = ini;
		while (aux->next != ini)
			aux = aux->next;
		aux->next = ini->next;
		free(ini);
		ini = aux->next;
	}
	return ini;
}