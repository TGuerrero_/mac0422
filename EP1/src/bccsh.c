#include <includes.h>
#include <syscalls.h>

#define MAX_ARGS_SIZE 10
#define MAX_PROMPT_SIZE 80
#define MAX_PATH_SIZE 20

void build_prompt(char *prompt) {
	char *user = getenv("USER"), *directory = NULL;
	directory = getcwd(directory, 0);
	prompt[0] = '{';
	strcat(prompt, user);
	strcat(prompt, "@");
	strcat(prompt, directory);
	strcat(prompt, "} ");
}

char * read_command() {
	char* buf = NULL, prompt[MAX_PROMPT_SIZE] = "";
	build_prompt(prompt);
    buf = readline(prompt);
	if (strlen(buf) > 0)
		add_history(buf);
	return buf;
}

void parse(char * command, char ** parsed) {
	for (int i=0; i < MAX_ARGS_SIZE; i++){
		parsed[i] = NULL;
	}

	for (int i = 0; command != NULL; i++) {
		parsed[i] = strsep(&command, " ");
	}
}

void exec_bin(char** parsed){
	pid_t child = fork();
	int process_status;
	char*parsed_path = parsed[0], path[MAX_PATH_SIZE] = "/usr/bin/";

	if (parsed_path[0] != '/' && parsed_path[0] != '.'){
		//Corrige o path, se necessário
		strcat(path, parsed_path);
	}
	else{
		strcpy(path, parsed_path);
	}

	if (child == -1) {
		printf("Erro: Não foi possível criar um novo processo.\n");
		exit(1);
	}
	else if (child == 0) {
		//Child
		// execve(filepath, argv[], envp[])
		execve(path, parsed, NULL);
		sleep(1);
	}
	else {
		//Father
		// printf("\n<Pid do filho: %d>\n", child);
		waitpid(child, &process_status, WUNTRACED);
		char * result = (WIFEXITED(process_status) == 1 ? "SUCESSO" : "FALHA");
		printf("\n<Processo Filho finalizado com %s>\n", result);
	}
}

void execute(char ** parsed) {
	char *path = parsed[0];
	if (strlen(path) == 0)
		return;
	if (!strcmp(path, "mkdir"))
		run_mkdir(parsed);
	else if (!strcmp(path,"kill"))
		run_kill(parsed);
	else if (!strcmp(path, "ln"))
		run_ln(parsed);
	else if (!strcmp(path, "cd"))
		run_cd(parsed);
	else 
		exec_bin(parsed);
}


int main (int argc, char **argv) {
	while (1) {
		char * raw_command;
		char * parsed[MAX_ARGS_SIZE];

		raw_command = read_command();
		parse(raw_command, parsed);
		execute(parsed);
		free(raw_command);
	}
}