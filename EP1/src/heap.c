#include <includes.h>
#include <heap.h>

void build_heap(Process *heap[], int n){
    for (int i=(n/2)-1; i >= 0; i--){
        sieve(heap, i, n);
    }
}

void sieve(Process *heap[], int node, int n){
    int child = 2*node + 1;
    Process *aux;
    while (child < n){
        if (child+1 < n && heap[child]->dt > heap[child+1]->dt)
            child++;
        if (heap[child]->dt > heap[node]->dt)
            break;
        aux = heap[child];
        heap[child] = heap[node];
        heap[node] = aux;
        node = child;
        child = 2*node + 1;
    }
}