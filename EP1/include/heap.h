#ifndef HEAP_H
#define HEAP_H
#include "process.h"

void build_heap(Process *heap[], int n);

void sieve(Process *heap[], int node, int n);

#endif