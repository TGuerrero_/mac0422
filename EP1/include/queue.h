#ifndef QUEUE_H
#define QUEUE_H

#include "process.h"

typedef struct LL {
	Process * process;
	struct LL * next;
} thread_cell;

thread_cell * add_thread(thread_cell * ini, Process * process);

thread_cell * remove_thread(thread_cell * ini);

#endif
