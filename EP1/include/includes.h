#ifndef INCLUDES_H
#define INCLUDES_H

#define ERROR -1
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <dirent.h>
#include <time.h>
#include <pthread.h> 
#include <sys/time.h>
#include <sched.h>

#endif