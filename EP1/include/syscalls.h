#ifndef SYSCALLS_H
#define SYSCALLS_H

#include <includes.h>

void run_mkdir(char ** parsed);

void run_kill(char ** parsed);

void run_ln(char ** parsed);

void run_cd(char ** parsed);

#endif