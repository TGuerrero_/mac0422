#ifndef PROCESS_H
#define PROCESS_H

#include <includes.h>

#define MAX_PROCESS_NAME_SIZE 30
#define MAX_PROCESSES 500

typedef struct {
	char name[MAX_PROCESS_NAME_SIZE];
	int t0;
	int dt;
	int deadline;
	pthread_mutex_t thread_mutex;
	pthread_t tid;
	int finalized;
} Process;

#endif