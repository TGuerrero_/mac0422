#!/bin/bash

gcc -o generate_stats generate_stats.c -lm

for size in 1 2 3
do
	for scheduler in 1 2 3
	do
		for person in 0 1
		do
			./generate_stats $size $scheduler $person
		done
	done
done