#!/bin/bash
cd ..
make

mkdir ./outputs
mkdir ./outputs/medium
for type in {1..3};
do
	mkdir ./outputs/medium/$type
    for i in {1..30};
    do
        echo "Tentativa número $i do medium"
        ./ep1 $type mediumInput.txt /medium/$type/mediumInput$i.txt
    done
done