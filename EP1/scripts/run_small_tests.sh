#!/bin/bash
cd ..
make

mkdir ./outputs
mkdir ./outputs/small
for type in {1..3};
do
	mkdir ./outputs/small/$type
    for i in {1..30};
    do
        echo "Tentativa número $i do small"
        ./ep1 $type smallInput.txt /small/$type/smallInput$i.txt
    done
done