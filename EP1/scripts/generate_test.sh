#!/bin/bash

gcc -o trace_generator trace_generator.c

small=1
medium=2
large=3

for traceSize in {1..3};
do
	./trace_generator $traceSize
done