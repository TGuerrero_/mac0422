#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAX 305
#define NUM_MEASURES 30

int main (int argc, char * argv[]) {
	int processList[MAX];
	
	char pathProcess[100];
	char pathLog[100];
	int fileSize;
	if (argv[1][0] == '1') {
		fileSize = 10;
		strcpy(pathProcess, "../inputs/smallInput.txt");
		if (argv[2][0] == '1') {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/SmallInput/FCFS/smallFCFS-Chico/smallInput");
			else
				strcpy(pathLog, "../Tests/SmallInput/FCFS/smallFCFS-Guerrero/smallInput");
		}
		else if (argv[2][0] == '2') {		
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/SmallInput/SRTN/smallSRTN-Chico/smallInput");
			else
				strcpy(pathLog, "../Tests/SmallInput/SRTN/smallSRTN-Guerrero/smallInput");
		}
		else {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/SmallInput/RR/smallRR-Chico/smallInput");
			else
				strcpy(pathLog, "../Tests/SmallInput/RR/smallRR-Guerrero/smallInput");
		}
	}
	else if (argv[1][0] == '2') {
		fileSize = 100;
		strcpy(pathProcess, "../inputs/mediumInput.txt");
		if (argv[2][0] == '1') {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/MediumInput/FCFS/mediumFCFS-Chico/mediumInput");
			else
				strcpy(pathLog, "../Tests/MediumInput/FCFS/mediumFCFS-Guerrero/mediumInput");
		}
		else if (argv[2][0] == '2') {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/MediumInput/SRTN/mediumSRTN-Chico/mediumInput");
			else
				strcpy(pathLog, "../Tests/MediumInput/SRTN/mediumSRTN-Guerrero/mediumInput");
		}
		else {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/MediumInput/RR/mediumRR-Chico/mediumInput");
			else
				strcpy(pathLog, "../Tests/MediumInput/RR/mediumRR-Guerrero/mediumInput");
		}
	}
	else {		
		fileSize = 300;
		strcpy(pathProcess, "../inputs/largeInput.txt");
		if (argv[2][0] == '1') {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/LargeInput/FCFS/largeFCFS-Chico/largeInput");
			else
				strcpy(pathLog, "../Tests/LargeInput/FCFS/largeFCFS-Guerrero/largeInput");
		}
		else if (argv[2][0] == '2') {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/LargeInput/SRTN/largeSRTN-Chico/largeInput");
			else
				strcpy(pathLog, "../Tests/LargeInput/SRTN/largeSRTN-Guerrero/largeInput");
		}
		else {
			if (argv[3][0] == '1')
				strcpy(pathLog, "../Tests/LargeInput/RR/largeRR-Chico/largeInput");
			else
				strcpy(pathLog, "../Tests/LargeInput/RR/largeRR-Guerrero/largeInput");
		}
	}


	// ./generate_stats [Small: 1 | Medium: 2 | Large: 3] [FCFS: 1 | SRTN: 2 | RR: 3]
	int ID, t0, dt, deadline, tf, tc;

	FILE *log, *process;
	process = fopen(pathProcess, "r");
	for (int i = 0; i <= fileSize; i++) {
		fscanf(process, "process%d %d %d %d\n", &ID, &t0, &dt, &deadline);
		processList[i] = deadline;
	}

	int deadlinesMetList[MAX];
	int contextChangesList[MAX];
	for (int measure = 1; measure <= NUM_MEASURES; measure++) {
		char curPathLog[100];
		sprintf(curPathLog, "%s%d.txt", pathLog, measure);
		log = fopen(curPathLog, "r");

		int deadlinesMet = 0;

		for (int i = 0; i < fileSize; i++) {
			fscanf(log, "process%d %d %d\n", &ID, &tf, &tc);
			if (processList[ID] <= tf) {
				deadlinesMet++;
			}
		}
		deadlinesMetList[measure] = deadlinesMet;
		fscanf(log, "%d", contextChangesList + measure);

		fclose(log);
	}

	//for (int i = 1; i <= NUM_MEASURES; i++) {
	//	printf("deadlinesMetList[%d]: %d\n", i, deadlinesMetList[i]);
	//}
	//for (int i = 1; i <= NUM_MEASURES; i++) {
	//	printf("contextChangesList[%d]: %d\n", i, contextChangesList[i]);
	//}

	// mean estimator: X^{prime} = sum_1^n(X[i]) / n
	double meanDeadlines = 0;
	double meanContextChanges = 0;
	for (int i = 1; i <= NUM_MEASURES; i++) {
		meanDeadlines += (double)deadlinesMetList[i];
		meanContextChanges += (double)contextChangesList[i];
	}
	meanDeadlines /= NUM_MEASURES;
	meanContextChanges /= NUM_MEASURES;


	// variance estimator: s = sqrt(1 / (n - 1) * sum_1^n(X[i] - mean))
	double varianceEstimatorDeadline = 0;
	double varianceEstimatorContextChange = 0;
	for (int i = 1; i <= NUM_MEASURES; i++) {
		varianceEstimatorDeadline += ((double)deadlinesMetList[i] - meanDeadlines)*((double)deadlinesMetList[i] - meanDeadlines);
		varianceEstimatorContextChange += ((double)contextChangesList[i] - meanContextChanges)*((double)contextChangesList[i] - meanContextChanges);
	}
	varianceEstimatorDeadline *= (1/((double)NUM_MEASURES-1));
	varianceEstimatorContextChange *= (1/((double)NUM_MEASURES-1));
	varianceEstimatorDeadline = sqrt(varianceEstimatorDeadline);
	varianceEstimatorContextChange = sqrt(varianceEstimatorContextChange);

	double ladoEsquerdoDeadline = meanDeadlines - 2.042*varianceEstimatorDeadline;
	double ladoDireitoDeadline = meanDeadlines + 2.042*varianceEstimatorDeadline;
	
	double ladoEsquerdoContextChange = meanContextChanges - 2.042*varianceEstimatorContextChange;
	double ladoDireitoContextChange = meanContextChanges + 2.042*varianceEstimatorContextChange;

	char * size = argv[1][0] == '1' ? "Small" : argv[1][0] == '2' ? "Medium" : "Large"; 
	char * scheduler = argv[2][0] == '1' ? "FCFS" : argv[2][0] == '2' ? "SRTN" : "RR"; 
	char * person = argv[3][0] == '1' ? "Chico" : "Guerrero";
	printf("\nPara uma quantidade %s de processos, o escalonador %s\n"
			"Aplicado aos outputs do %s\n"
			" tem média de %lf cumprimentos de deadline com intervalo de confiança [%lf;%lf]\n"
			" e média de %lf mudanças de contexto com intervalo de confiança [%lf;%lf]\n"
			, size, scheduler, person, meanDeadlines, ladoEsquerdoDeadline, ladoDireitoDeadline,
			meanContextChanges, ladoEsquerdoContextChange, ladoDireitoContextChange);

	fclose(process);
}