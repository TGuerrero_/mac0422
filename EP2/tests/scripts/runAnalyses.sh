#!/bin/bash

cyclistNames=(smallCyclist mediumCyclist largeCyclist)
trackNames=(smallTrack mediumTrack largeTrack)

gcc -o ./tests/scripts/statsGenerator ./tests/scripts/statsGenerator.c -lm

mkdir ./tests/results -p

for indexA in 0 1 2; do
	for indexB in 0 1 2; do
		mkdir ./tests/results/${trackNames[$indexA]}/${cyclistNames[$indexB]} -p
		./tests/scripts/statsGenerator ./tests/outputs/${trackNames[$indexA]}/${cyclistNames[$indexB]} > ./tests/results/${trackNames[$indexA]}/${cyclistNames[$indexB]}/result.txt
	done
done