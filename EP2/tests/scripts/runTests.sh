#!/bin/bash

make clean

make

mkdir ./tests/outputs/smallTrack -p
mkdir ./tests/outputs/mediumTrack -p
mkdir ./tests/outputs/largeTrack -p

cyclistSizes=(10 50 100)
cyclistNames=(smallCyclist mediumCyclist largeCyclist)

trackSizes=(250 500 1000)
trackNames=(smallTrack mediumTrack largeTrack)

for indexA in 0 1 2; do
	echo Cyclist quantity: ${cyclistNames[$indexA]}
	for indexB in 0 1 2; do
		mkdir ./tests/outputs/${trackNames[$indexB]}/${cyclistNames[$indexA]} -p
		echo Track size: ${trackNames[$indexB]}
		for times in {1..30}; do
			echo Attempt number: $times
			/usr/bin/time -f "\n%S CPU-seconds\n%e Elapsed Real Time\n%K Memory in KiloBytes" --output=./tests/outputs/${trackNames[$indexB]}/${cyclistNames[$indexA]}/TIME$times.txt ./ep2 ${trackSizes[$indexB]} ${cyclistSizes[$indexA]} /${trackNames[$indexB]}/${cyclistNames[$indexA]}/$times -noprint
		done
	done
done