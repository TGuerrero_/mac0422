#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define NUM_MEASURES 30

int main (int argc, char * argv[]) {
	FILE * logTime;	
	FILE * logMemory;	

	char folder[70];
	char fileTime[70];
	char fileMemory[70];

	printf("%s\n", argv[1]);
	strcpy(folder, argv[1]);
	double cpuTime[NUM_MEASURES+1];
	double realTime[NUM_MEASURES+1];
	double memUsage[NUM_MEASURES+1];
	double cpuTimeTotal = 0, realTimeTotal = 0, memUsageTotal = 0;

	for (int i = 1; i <= 30; i++) {
		sprintf(fileTime, "%s/TIME%d.txt", folder, i);
		sprintf(fileMemory, "%s/%dMEMORY.txt", folder, i);
		logTime = fopen(fileTime, "r");
		logMemory = fopen(fileMemory, "r");

		fscanf(logTime, "\n%lf CPU-seconds\n%lf Elapsed Real Time", cpuTime + i, realTime + i);
		fscanf(logMemory, "Total memory used: %lf", memUsage + i);
		cpuTimeTotal += cpuTime[i];
		realTimeTotal += realTime[i];
		memUsageTotal += memUsage[i];

		fclose(logTime);
		fclose(logMemory);
	}
	double averageCPUTime = cpuTimeTotal/NUM_MEASURES;
	double averageRealTime = realTimeTotal/NUM_MEASURES;
	double averageMemoryUsage = memUsageTotal/NUM_MEASURES;

	// variance estimator: s = sqrt(1 / (n - 1) * sum_1^n(X[i] - mean))
	double varianceEstimatorCPUTime = 0;
	double varianceEstimatorRealTime = 0;
	double varianceEstimatorMemoryUsage = 0;
	for (int i = 1; i <= NUM_MEASURES; i++) {
		varianceEstimatorCPUTime += (cpuTime[i] - averageCPUTime)*(cpuTime[i] - averageCPUTime);
		varianceEstimatorRealTime += (realTime[i] - averageRealTime)*(realTime[i] - averageRealTime);
		varianceEstimatorMemoryUsage += (memUsage[i] - averageMemoryUsage)*(memUsage[i] - averageMemoryUsage);
	}

	varianceEstimatorCPUTime *= (1/((double)NUM_MEASURES-1));
	varianceEstimatorRealTime *= (1/((double)NUM_MEASURES-1));
	varianceEstimatorMemoryUsage *= (1/((double)NUM_MEASURES-1));

	varianceEstimatorCPUTime = sqrt(varianceEstimatorCPUTime);
	varianceEstimatorRealTime = sqrt(varianceEstimatorRealTime);
	varianceEstimatorMemoryUsage = sqrt(varianceEstimatorMemoryUsage);

	double ladoEsquerdoCPUTime = averageCPUTime - 2.042*varianceEstimatorCPUTime;
	double ladoDireitoCPUTime = averageCPUTime + 2.042*varianceEstimatorCPUTime;

	double ladoEsquerdoRealTime = averageRealTime - 2.042*varianceEstimatorRealTime;
	double ladoDireitoRealTime = averageRealTime + 2.042*varianceEstimatorRealTime;
	
	double ladoEsquerdoMemoryUsage = averageMemoryUsage - 2.042*varianceEstimatorMemoryUsage;
	double ladoDireitoMemoryUsage = averageMemoryUsage + 2.042*varianceEstimatorMemoryUsage; 

	printf("Average CPU Time: %.3lf Seconds\n", averageCPUTime);
	printf("Average Real Time: %.3lf Seconds\n", averageRealTime);
	printf("Average Memory Usage: %.3lf MegaBytes\n", averageMemoryUsage/250);

	printf("Confidence interval CPU Time: [%.3lf : %.3lf]\n", ladoEsquerdoCPUTime, ladoDireitoCPUTime);
	printf("Confidence interval Real Time: [%.3lf : %.3lf]\n", ladoEsquerdoRealTime, ladoDireitoRealTime);
	printf("Confidence interval Memory Usage: [%.3lf : %.3lf]\n", ladoEsquerdoMemoryUsage/250, ladoDireitoMemoryUsage/250);
} 