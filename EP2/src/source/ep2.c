#include <externals.h>
#include <atomic.h>
#include <cyclists.h>
#include <linked_list.h>
#include <pista.h>
#include <aux.h>

const int MAX_FILE_NAME_SIZE = 70;

FILE * output;
_Atomic int currentLap = 0;
_Atomic int ** ranks;
Cell ** broke;
int qtdBroke = 0;

//flags
/* LastLap:
0 - Ninguém chegou nas ultimas voltas
1 - Alguém acabou de chegar e vai sortear
2 - Ninguém está a 90km/h
3 - Alguém está a 90km/h
*/
_Atomic int lastLap = 0;

// Thread Ciclista
void * ciclista(void * param) {
	int isNewLap = 0;
	int time = 0;
	Cyclist *cyclist = param;

	while (!cyclist->eliminated && remaining > 1) {
		if (isNewLap) {
			//Nova volta
			isNewLap = 0;
			if (cyclistNewLap(pista, d, cyclist, numLaps - 2*qtdBroke - 2)) {
				pthread_mutex_lock(&breaking);
				qtdBroke++;
				broke[cyclist->lap/6] = push(broke[cyclist->lap/6], cyclist);
				pthread_mutex_unlock(&breaking);
				if (cyclist->lap == currentLap) {
                	remaining--;
            	}
				printf("VOLTA %d\n"
						"O ciclista de identificador %d quebrou\n\n", cyclist->lap, cyclist->id);
				fprintf(output, "VOLTA %d\n"
						"O ciclista %d quebrou\n\n", cyclist->lap, cyclist->id);
            	return NULL;
			}
		}
		
		if (lastLap == 3) {
			//Simulação em intervalos de 20ms
			time += 20;
			isNewLap = race(1, cyclist, ranks, &lastLap, numLaps - 2*qtdBroke - 2);
		}
		else {
			//Simulação em intervalos de 60ms
			time += 60;
			isNewLap = race(0, cyclist, ranks, &lastLap, numLaps - 2*qtdBroke - 2);
		}

		if (isNewLap) {
			cyclist->time += time;
			time = 0;
		}

		cyclist->waiting = 1;
		cyclist->arrive = 1;
		//barreira
		while (cyclist->proceed == 0) {
			usleep(0.01);
		}

		if (isNewLap && (cyclist->lap + 1) >= (numLaps - 2*qtdBroke)) {
			// Terminou a corrida
			// Lazy stop
			cyclist->eliminated = 1;
		}

		if (cyclist->eliminated) {
			pistaSet(pista, cyclist->position_i, cyclist->position_j, -1);
			cyclist->arrive = 1;
		}
		else {
			cyclist->proceed = 0;
		}
	}

	return NULL;
}

int main(int argc, char * argv[]) {
	// ./ep2 <d> <n> <OUTPUT> <desenho> [SEED]
	int debugFlag;
	char outputFile[MAX_FILE_NAME_SIZE];

	if (argc < 5) {
		printf("Por favor, insira todos os parâmetros na seguinte ordem: \n"
				" ./ep2 <d> <n> <OUTPUT_NAME> <FLAGD> [SEED]\n");
		exit(1);
	}

	// Inicializa a seed
	if (argc == 6)
		srand(atoi(argv[5]));
	else
		srand(time(NULL));

	// Flag de debug
	if (!strcmp(argv[4], "-noprint"))
		debugFlag = 0;
	else if (!strcmp(argv[4], "-print"))
		debugFlag = 1;
	else if (!strcmp(argv[4], "-fancyprint"))
		debugFlag = 2;
	else {
		printf("Flag de debug errada! Escolha uma das opções abaixo: \n"
				"-print || -noprint || -fancyprint\n");
		exit(1);
	}

	// Adiciona o path completo
	sprintf(outputFile, "./tests/outputs/%s", argv[3]);

	// Abre os arquivos de output
	output = fopen(outputFile, "w");

	// Inicializa a pista
	pistaInit(atoi(argv[1])); //argv[1] == d

	//Inicializa o vetor de ciclistas e coloca todos os ciclistas na posição inicial
	cyclistsInit(qtdCyclistTracks, pista, d, atoi(argv[2])); //argv[2] == n
	numLaps = 2*(n-1);

	//Inicia o vetor que conta o número de ciclistas que passaram por uma volta
	lapsInit(2*(n-1));

	//Inicia o vetor que contabiliza o número de ciclistas quebrados
	broke = malloc (((numLaps/6) + 1) * sizeof(Cell *));
	for (int i = 0; i < ((numLaps/6) + 1); i++) {
		broke[i] = NULL;
	}

	//Inicializa a matriz de classificação
	ranks = malloc(numLaps * sizeof(_Atomic int *));
	for (int i = 0; i < numLaps; i++) {
		ranks[i] = malloc((n - i/2) * sizeof(_Atomic int));
		for (int j = 0; j < (n - i/2); j++)
			ranks[i][j] = -1;
	}

	printf("**** COMEÇOU A CORRIDA *****\n");
	if (debugFlag == 2) {
		fprintf(stderr, "\n-------------------------------\n");
	}

	for (int i = 0; i < n; i++) {
		pthread_create(&cyclists[i]->tid, NULL, ciclista, cyclists[i]);
	}

	//Simulador
	while (remaining > 1) {
		// Espera a barreira
		for (int i = 0; i < n; i++) {
			if (cyclists[i]->eliminated == 0) {
				while (cyclists[i]->arrive == 0) {
					usleep(0.01);
				}
				cyclists[i]->arrive = 0;
			}
		}

		// Libera os waitings antes de liberar a barreira
		for (int i = 0; i < n; i++) {
			if (cyclists[i]->eliminated == 0) 
				cyclists[i]->waiting = 0;
		}
		
		//Printa a situação da pista
		if (debugFlag == 1) {
			printTrack();
		}
		else if (debugFlag == 2) {
			printFancyTrack();
		}

		//Último ciclista fez uma volta
		if (laps[currentLap] >= remaining) {
			if ((currentLap % 2) == 1) {
				//Volta eliminatória
				Cell * lastCyclists = NULL;
				Cyclist * eliminated;
				int eliminatedNum;

				for (int i = 0; i < n; i++) {
					if (!cyclists[i]->eliminated && cyclists[i]->lap == currentLap) {
						//Provável eliminado
						lastCyclists = push(lastCyclists, cyclists[i]);
					}
				}

				if (lastCyclists == NULL) {
					//Eliminado já está em voltas a frente, logo, pegamos o ultimo que passou pela linha de chegada
					eliminated = cyclists[ranks[currentLap][remaining-1]];
				}
				else {
					if (lastCyclists->size > 1) {
						eliminatedNum = rand() % lastCyclists->size;
						eliminated = getter(lastCyclists, eliminatedNum);
					}
					else {
						eliminated = getter(lastCyclists, 0);
					}
				}
				printf("VOLTA %d\n"
						"O ciclista %d foi eliminado\n\n", currentLap, eliminated->id);
				fprintf(output, "VOLTA %d\n"
								"O ciclista %d foi eliminado\n\n", currentLap, eliminated->id);
				eliminated->eliminated = 1;
				remaining--;
				lastCyclists = destroy(lastCyclists);
				
				// Garante que o eliminado está na ultima posição do ranks da volta
				int aux = eliminated->id;
				int rankIndex;
				for (rankIndex = 0; rankIndex < n - (currentLap)/2; rankIndex++){
					if (ranks[currentLap][rankIndex] == eliminated->id)
						break;
				}

				if (ranks[currentLap][n - (currentLap/2 + 1)] != -1) {
					ranks[currentLap][rankIndex] = ranks[currentLap][n - (currentLap/2 + 1)];
				}
				ranks[currentLap][n - (currentLap/2 + 1)] = aux;
				//###

				eliminated->proceed = 1;
			}

			// Printa a classificação em cada volta
			rankPerLap(output, currentLap, ranks);
			currentLap++;

			// Tira os ciclistas que já quebraram nesta volta
			if (currentLap < numLaps && (currentLap % 6 == 0) && broke[currentLap/6] != NULL) {
				remaining -= broke[currentLap/6]->size;
			}
		}

		// Dá 90km/h a um dos dois ultimos competidores
		if (lastLap == 1){
			int overcharge = rand() % 10;
			if (overcharge == 0) {
				int speed = rand() % 2;
				int firstCyclist = ranks[numLaps - 2*qtdBroke - 3][0]; //lastLaps - 1
				for (int i = 0, set = 0; i < n && !set; i++) {
					if (!cyclists[i]->eliminated) {
						if (cyclists[i]->id == firstCyclist && speed == 0) {
							// Boost para o primeiro lugar
							cyclists[i]->boost = 1;
							set = 1;
						} 
						else if (cyclists[i]->id != firstCyclist && speed == 1) {
							cyclists[i]->boost = 1;
						}
					}
				}
				lastLap = 3;
			}
			else
				lastLap = 2;
		}

		// Libera a barreira
		for (int i = 0; i < n; i++) {
			if (cyclists[i]->eliminated == 0) {	
				cyclists[i]->proceed = 1;
			}
		}
	}

	printf("**** TERMINOU A CORRIDA *****\n");
	finalPrint(output, currentLap, broke, ranks);
	fclose(output);

	// Libera memória alocada
	free(laps);
	pistaDestroy();
	cyclistsDestroy();

	for(int i = 0; i < numLaps/6; i++) {
		broke[i] = destroy(broke[i]);
	}
	free(broke);

	for(int i = 0; i < numLaps; i++){
		free(ranks[i]);
	}
	free(ranks);
	return 0;
}