#include <aux.h>

// Printa o ranqueamento final e todos os ciclistas que quebraram.
void finalPrint(FILE * output, int currentLap, Cell ** broke, _Atomic int ** ranks) {
	printf("\nRanqueamento final: \n");
	fprintf(output, "\nRanqueamento final: \n");

	printf("1º lugar: Ciclista %d em %.2lf segundos\n", 
		ranks[currentLap-1][0], (double)cyclists[ranks[currentLap-1][0]]->time/1000);
	fprintf(output, "1º lugar: Ciclista %d em %.2lf segundos\n", 
		ranks[currentLap-1][0], (double)cyclists[ranks[currentLap-1][0]]->time/1000);

	for (int rank = 2, i = currentLap-1; i > 0; i -= 2, rank++) {
		printf("%dº lugar: Ciclista %d em %.2lf segundos\n",
			rank,  ranks[i][n - (i/2 + 1)], (double)cyclists[ranks[i][n - (i/2 + 1)]]->time/1000);
		fprintf(output, "%dº lugar: Ciclista %d em %.2lf segundos\n",
			rank,  ranks[i][n - (i/2 + 1)], (double)cyclists[ranks[i][n - (i/2 + 1)]]->time/1000);
	}

	printf("\nCiclistas quebrados: \n");
	fprintf(output, "\nCiclistas quebrados: \n");
	for (int i = 0; i < numLaps/6; i++) {
		while (broke[i] != NULL) {
			printf("Quebrou: Ciclista %d - Volta %d\n", broke[i]->cyclist->id, (6*i));
			fprintf(output, "Quebrou: Ciclista %d - Volta %d\n", broke[i]->cyclist->id, (6*i));
			broke[i] = pop(broke[i]);
		}
	}
}

// Printa a situação atual da pista
void printTrack() {
	fprintf(stderr, "\n-------------------------------\n");
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < d; j++) {
			if (pistaGet(pista, i, j) == -1){
				fprintf(stderr, "\033[1;30m");
				for (int k = n; k != 0; k = k/10)
					fprintf(stderr, "*");
				fprintf(stderr, " ");
				fprintf(stderr, "\033[1;0m");	
			}
			else{
				int zeros = 0;
				for (int k = n; k != 0; k = k/10)
					zeros++;
				for (int k = pistaGet(pista, i, j); k != 0; k = k/10)
					zeros--;
				if (pistaGet(pista, i, j) == 0)
					zeros--;
				for (int k = 0; k < zeros; k++)
					fprintf(stderr, "0");
				fprintf(stderr, "\033[1;3%dm", cyclists[pistaGet(pista, i, j)]->color);
				fprintf(stderr, "%d ", pistaGet(pista, i, j));
				fprintf(stderr, "\033[1;0m");
			}
		}
		fprintf(stderr, "\n");
	}
}

// Printa a situação atual da pista sem repetição de pista
void printFancyTrack() {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < d; j++) {
			if (pistaGet(pista, i, j) == -1){
				fprintf(stderr, "\033[1;30m");
				for (int k = n; k != 0; k = k/10)
					fprintf(stderr, "*");
				fprintf(stderr, " ");
				fprintf(stderr, "\033[1;0m");	
			}
			else{
				int zeros = 0;
				for (int k = n; k != 0; k = k/10)
					zeros++;
				for (int k = pistaGet(pista, i, j); k != 0; k = k/10)
					zeros--;
				if (pistaGet(pista, i, j) == 0)
					zeros--;
				for (int k = 0; k < zeros; k++)
					fprintf(stderr, "0");
				fprintf(stderr, "\033[1;3%dm", cyclists[pistaGet(pista, i, j)]->color);
				fprintf(stderr, "%d ", pistaGet(pista, i, j));
				fprintf(stderr, "\033[1;0m");
			}
		}
		fprintf(stderr, "\n");
	}
	fprintf(stderr, "\r");
		for (int i = 0; i < 10; i++){
			fprintf(stderr, "\033[A");
	}
}

// Printa a classificação em cada volta
void rankPerLap (FILE * output, int currentLap, _Atomic int ** ranks) {
    printf("Classificação por identificador após a volta %d: \n", currentLap);
    fprintf(output, "Classificação por identificador após a volta %d: \n", currentLap);
    for (int i = 0; i < laps[currentLap]; i++) {
        printf("%dº: %d", i+1, ranks[currentLap][i]);
        fprintf(output, "%dº: %d", i+1, ranks[currentLap][i]);
        if (i != laps[currentLap] - 1) {
            printf(" | ");
            fprintf(output, " | ");
        }
        else {
            printf("\n");
            fprintf(output, "\n");
        }
    }
}

unsigned long long int getMemoryStats() {
	FILE * memOut;
	char buffer[1000];
	char * memory;
	unsigned long long int total;

	memOut = fopen("/proc/self/statm", "r");

	fread(buffer, 1000, 1, memOut);

	memory = strtok(buffer, " ");

	total = atoi(memory);

	for (int i = 0; i < 4; i++) {
		memory = strtok(NULL, " ");
	}

	total += atoi(memory);

	fclose(memOut);

	return total;
}