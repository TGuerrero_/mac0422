#include <pista.h>

/*
* pistaInit()
* Inicializa uma matriz com 10 linhas e d colunas com todas as posições vazias(-1)
*/
void pistaInit(int distance) {
    d = distance;
    pista = malloc(10*sizeof(int *));
	for (int i = 0; i < 10; i++) {
		pista[i] = malloc(d*sizeof(int));
		for (int j = 0; j < distance; j++) {
			pista[i][j] = -1;
		}
	}

    for (int i = 0; i < 10; i++) {
        qtdCyclistTracks[i] = 0;
    }

    mutexesInit(d);
}

/*
* lapsInit()
* Inicia o vetor que conta o número de ciclistas que passaram por uma volta
*/
void lapsInit(int lapsSize) {
    numLaps = lapsSize;
    laps = malloc (numLaps * (sizeof(_Atomic int)));
	for (int i = 0; i < numLaps; i++){
		laps[i] = 0;
	}
}

void pistaDestroy() {
    for (int i = 0; i < 10; i++) {
		free(pista[i]);
	}
	free(pista);
    pista = NULL;

    mutexesDestroy(d);
}

/*
* quickRaceCheckout()
* Checa se o ciclista pode se mover em uma corrida com
* simulando iterações de 20ms.
* Retorna 1 se ele pode se mover e 0 caso contrário
*/
int quickRaceCheckout(Cyclist *cyclist) {
    return ((cyclist->velocity == 90 && cyclist->halfway == 1) ||
        (cyclist->velocity == 60 && cyclist->halfway == 3) ||
        (cyclist->velocity == 30 && cyclist->halfway == 6));
}

/*
* normalRaceCheckout()
* Checa se o ciclista pode se mover em uma corrida com
* simulando iterações de 60ms.
* Retorna 1 se ele pode se mover e 0 caso contrário
*/
int normalRaceCheckout(Cyclist *cyclist) {
    return (cyclist->velocity == 60 || cyclist->halfway);
}

/*
* int quickRace()
* Simula uma iteração de um ciclista.
* simulationType = 0 | Iteração de 60ms
* simulationType = 1 | Iteração de 20ms
* Retorna se o ciclista começou uma volta nova
*/
int race(int simulationType, Cyclist *cyclist, _Atomic int ** ranks, _Atomic int *lastLapFlag, int lastLaps) {
    int isNewLap = 0;
    if ((simulationType == 0 && normalRaceCheckout(cyclist)) ||
        (simulationType == 1 && quickRaceCheckout(cyclist)))
        {
        int isMoving = checkForward(cyclist);

        // Ultrapassagem
        if (isMoving == -1) {
            // Talvez possa ultrapassar
            isMoving = checkSurpass(cyclist);
        }

        if (isMoving != -1) {
            if (isMoving == 0) {
                isNewLap = moveCyclist(cyclist, cyclist->position_i, cyclist->position_j + 1);
            }
            else if (isMoving > 0) {
                isNewLap = moveCyclist(cyclist, isMoving, cyclist->position_j + 1);
                unlockTrack(cyclist->position_i);
            }
            unlockPistaAt(cyclist->position_i, cyclist->position_j);
            if (isNewLap) {
                //Terminou uma volta
                if (cyclist->lap < numLaps) {
                    //Insere a classificação do ciclista durante a volta "cyclist->lap"
                    ranks[cyclist->lap][laps[cyclist->lap]] = cyclist->id;
                    laps[cyclist->lap]++;

                    if ((*lastLapFlag) == 0 && (cyclist->lap + 1) == lastLaps) {
                        // Entrou nas duas ultimas voltas
                        (*lastLapFlag) = 1;
                    }
                }
            }
        }
    }
    else {
        if (simulationType == 0 && cyclist->velocity == 30)
            cyclist->halfway = 1;
        else if (simulationType == 1)
            cyclist->halfway++;
    }

    return isNewLap;
}

/*
* int moveCyclist()
* Recebe a nova posição de um ciclista e avança o mesmo para a posição.
* Retorna se o ciclista começou uma volta nova
*/
int moveCyclist(Cyclist *cyclist, int newPosition_i, int newPosition_j) {
    int isNewLap = 0;
    cyclist->halfway = 0;
    //Avança na pista
    pista[newPosition_i][newPosition_j % d] = cyclist->id;
    pistaSet(pista, cyclist->position_i, cyclist->position_j, -1);

    if (cyclist->position_i != newPosition_i) {
        qtdCyclistTracks[cyclist->position_i]--;
        qtdCyclistTracks[newPosition_i]++;
    }

    cyclist->position_i = newPosition_i;
    cyclist->position_j = (newPosition_j) % d;
    if (cyclist->position_j == 0) {
        //Terminou uma volta
        isNewLap = 1;
    }
    return isNewLap;
}

/*
* int checkForward()
* Checa se é posível avançar para alguma próxima posição
* Retorna 0 se é possível ou -1 caso contrário
*/
int checkForward(Cyclist *cyclist) {
    int isMoving = 0;
    // idProx == ID do ciclista na posição que o ciclista quer ir
    int idProx = pistaGet(pista, cyclist->position_i, (cyclist->position_j + 1) % d);
    //Espera a posição ficar livre
    while (isMoving == 0 && pistaGet(pista, cyclist->position_i, (cyclist->position_j + 1) % d) != -1) {
        // printf("\nCiclista %d | i: %d", cyclist->id, cyclist->position_i);
        fflush(NULL);
        if (qtdCyclistTracks[cyclist->position_i] == d || 
        (cyclists[idProx]->waiting && pistaGet(pista, cyclist->position_i, (cyclist->position_j + 1) % d) != -1)) {
            isMoving = -1;
        }
        usleep(0.01);
    }
    if (isMoving == 0) {
        lockPistaAt(cyclist->position_i, (cyclist->position_j + 1) % d);
        if (pista[cyclist->position_i][(cyclist->position_j + 1) % d] != -1) {
            unlockPistaAt(cyclist->position_i, (cyclist->position_j + 1) % d);
        }
    }

    return isMoving;
}

/*
* int surpass()
* Recebe a situação do ciclista e avalia se a sua ultrapassagem é possível
* Retorna a posição em que é possível a ultrapassagem ou -1 caso não seja possível ultrapassar
*/
int checkSurpass(Cyclist * cyclist) {
    for (int i = cyclist->position_i + 1; i < 10; i++) {
        lockTrack(i);
        lockPistaAt(i, (cyclist->position_j + 1) % d);
        if (pista[i][(cyclist->position_j + 1) % d] == -1 && qtdCyclistTracks[i] != d - 1) {
            for (int j = cyclist->position_i + 1; j < 10; j++) {
                if (pista[j][cyclist->position_j] == -1) {
                    return i;
                }
            }
        }
        unlockTrack(i);
        unlockPistaAt(i, (cyclist->position_j + 1) % d);
    }
    return -1;
}