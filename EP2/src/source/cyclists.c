#include <cyclists.h>

/*
* cyclistsInit()
* Inicializa o vetor de ciclistas e gera todos os ciclistas
* colocando cada um em sua posição inicial na pista
*/
void cyclistsInit(int qtdCyclistTracks[10], int ** pista, int d, int cyclistsSize) {
    n = cyclistsSize;
    remaining = n;
    
    //Inicializa o vetor de ciclistas
	cyclists = malloc(n*sizeof(Cyclist *));

    // Inicializa o semáforo que bloqueia duas quebras simultâneas
	pthread_mutex_init(&breaking, NULL);

	// Inicializa os ciclistas
	int currentColor = 1;
	int next_position[5] = {0, 0, 0, 0, 0};
	for (int i = 0; i < n; i++) {
		Cyclist* new = cyclistGenerate(i, currentColor);
		cyclists[new->id] = new;

        //Seta a próxima cor
	    currentColor = (currentColor % 7) + 1;

		//Organiza os ciclistas na pista
		int position = rand() % 5;
        while (next_position[position] == d)
            position = rand() % 5;
		pistaSet(pista, position, next_position[position], new->id);
		new->position_i = position;
        qtdCyclistTracks[position]++;
		new->position_j = next_position[position];
		next_position[position]++;
	}
}

/*
* cyclistGenerate()
* Gera um novo ciclista com as propriedades padrões
* Retorna um ponteiro pra esse ciclista
*/
Cyclist* cyclistGenerate(int id, int color) {
    Cyclist* new = malloc(sizeof(Cyclist));
    new->id = id;
    new->lap = 0;
    new->velocity = 30;
    new->halfway = 0;
    new->eliminated = 0;
    new->time = 0;
    new->waiting = 0;
    new->arrive = 0;
    new->proceed = 0;
    new->boost = 0;
    new->color = color;
    return new;
}

void cyclistsDestroy() {
    for (int i = 0; i < n; i++) {
		free(cyclists[i]);
	}
	free(cyclists);
    pthread_mutex_destroy(&breaking);
}

/*
* cyclistNewLap()
* Faz as operações necessárias após um ciclista mudar de volta
* Retorna se o ciclista quebrou naquela volta
*/
int cyclistNewLap(int ** pista, int d, Cyclist* cyclist, int lastLaps) {
    int eliminated = 0;
    cyclist->lap++;
    if (cyclist->lap == lastLaps && cyclist->boost)
        cyclist->velocity = 90;
    else if (cyclist->velocity != 90)
        sort_velocity(cyclist);

    if ((cyclist->lap) % 6 == 0) {
        //Possibilidade do ciclista quebrar
        eliminated = sort_breakdown();
        if (eliminated) {
            cyclist->arrive = 1;
            cyclist->eliminated = 1;
            pistaSet(pista, cyclist->position_i, cyclist->position_j, -1);
        }
    }
    return eliminated;
}

void sort_velocity(Cyclist* current) {
    int probability = rand() % 10;
    if (current->velocity == 30){
        if (probability >= 8)
            current->velocity = 30;
        else
            current->velocity = 60;
    }
    else {
        if (probability >= 6)
            current->velocity = 30;
        else
            current->velocity = 60;
    }
}

int sort_breakdown() {
    int broke = 0;
    if (remaining > 5){
        int probability = rand() % 100;
        if (probability < 5)
            broke = 1;
    }
    return broke;
}