#include <linked_list.h>

/*
* push()
* Insere no início da lista ligada
* Retorna um ponteiro para o novo início da lista
*/
Cell * push(Cell * current, Cyclist *cyclist) {
    Cell * new = malloc(sizeof(Cell));
    new->next = current;
    new->cyclist = cyclist;
    if (current != NULL)
        new->size = current->size + 1;
    else
        new->size = 1;
    return new;
}

/*
* pop()
* Remove o primeiro elemento da lista ligada
* Retorna o novo começo da lista ligada
*/
Cell * pop(Cell * current) {
    if (current == NULL)
        return NULL;
    Cell *newTop = current->next;
    free(current);
    return newTop;
}

/*
* getter()
* Retorna o cara que está na posição "selected" da lista ligada
*/
Cyclist * getter(Cell *current, int selected) {
    Cell * aux;
    for (aux = current; aux != NULL && selected > 0; aux = aux->next, selected--);
    if (aux == NULL) {
        return NULL;
    }
    return aux->cyclist;
}


Cell * destroy (Cell *current) {
    Cell *aux;
    while (current != NULL){
        aux = current;
        current = current->next;
        free(aux);
    }
    return NULL;
}