#include <atomic.h>

static pthread_mutex_t ** pistaMutexes;
static pthread_mutex_t trackMutexes[10];

void lockPistaAt(int i, int j) {
    pthread_mutex_lock(&pistaMutexes[i][j]);
}

void unlockPistaAt(int i, int j) {
    pthread_mutex_unlock(&pistaMutexes[i][j]);
}

void lockTrack(int i) {
    pthread_mutex_lock(&trackMutexes[i]);
}

void unlockTrack(int i) {
    pthread_mutex_unlock(&trackMutexes[i]);
}

void mutexesInit(int distance) {
    pistaMutexes = malloc(10*(sizeof(pthread_mutex_t *)));
    for (int i = 0; i < 10; i++) {
        pthread_mutex_init(&trackMutexes[i], NULL);
        pistaMutexes[i] = malloc(distance*(sizeof(pthread_mutex_t)));
        for (int j = 0; j < distance; j++) {
            pthread_mutex_init(&pistaMutexes[i][j], NULL);
        }
    }
}

void mutexesDestroy(int distance) {
    for (int i = 0; i < 10; i++) {
        pthread_mutex_destroy(&trackMutexes[i]);
        for (int j = 0; j < distance; j++)
            pthread_mutex_destroy(&pistaMutexes[i][j]);
        free(pistaMutexes[i]);
    }
    free(pistaMutexes);
}

int pistaGet(int ** pista, int i, int j) {
    pthread_mutex_lock(&pistaMutexes[i][j]);
    int answer = pista[i][j];
    pthread_mutex_unlock(&pistaMutexes[i][j]);
    return answer;
}

int trackGet(int track[], int i) {
    pthread_mutex_lock(&trackMutexes[i]);
    int answer = track[i];
    pthread_mutex_unlock(&trackMutexes[i]);
    return answer;
}

void pistaSet(int ** pista, int i, int j, int newValue) {
    pthread_mutex_lock(&pistaMutexes[i][j]);
    pista[i][j] = newValue;
    pthread_mutex_unlock(&pistaMutexes[i][j]);
}

void trackSet(int track[], int i, int acc) {
    pthread_mutex_lock(&trackMutexes[i]);
    track[i] += acc;
    pthread_mutex_unlock(&trackMutexes[i]);
}