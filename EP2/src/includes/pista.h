#ifndef PISTA_H
#define PISTA_H

#include <cyclists.h>
#include <atomic.h>

int ** pista;
int d;

int qtdCyclistTracks[10];
_Atomic int * laps;
_Atomic int numLaps;

void pistaInit(int distance);

void lapsInit(int lapsSize);

void pistaDestroy();

int quickRaceCheckout(Cyclist *cyclist);

int normalRaceCheckout(Cyclist *cyclist);

int race(int simulationType, Cyclist *cyclist, _Atomic int ** ranks, _Atomic int *lastLapFlag, int lastLaps);

int moveCyclist(Cyclist *cyclist, int newPosition_i, int newPosition_j);

int checkForward(Cyclist *cyclist);

int checkSurpass(Cyclist * cyclist);

#endif