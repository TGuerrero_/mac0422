#ifndef CYCLISTS_H
#define CYCLISTS_H

#include <externals.h>
#include <atomic.h>

typedef struct {
    /* data */
    int id;
    int lap;
    int velocity;
    int position_i;
    int position_j;
    int halfway;
    int eliminated;
    int time;
    int boost;
    int color;
    _Atomic int arrive;
    _Atomic int proceed;
    _Atomic int waiting;
    pthread_t tid;
} Cyclist;

Cyclist ** cyclists;
pthread_mutex_t breaking;
_Atomic int remaining;
int n;

void cyclistsInit(int qtdCyclistTracks[10], int ** pista, int d, int cyclistsSize);

Cyclist* cyclistGenerate(int id, int color);

void cyclistsDestroy();

int cyclistNewLap(int ** pista, int d, Cyclist* cyclist, int lastLaps);

void sort_velocity(Cyclist* current);

int sort_breakdown();

#endif