#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include <cyclists.h>

typedef struct cell{
    Cyclist * cyclist;
    struct cell * next;
    int size;
} Cell;

Cell * push(Cell * current, Cyclist *cyclist);

Cell * pop(Cell * current);

Cyclist * getter(Cell *current, int n);

Cell * destroy (Cell *current);

#endif