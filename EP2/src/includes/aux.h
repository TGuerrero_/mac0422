#ifndef AUX_H
#define AUX_H
#include <externals.h>
#include <pista.h>
#include <linked_list.h>

void finalPrint(FILE * output, int currentLap, Cell ** broke, _Atomic int ** ranks);

void printTrack();

void printFancyTrack();

void rankPerLap (FILE * output, int currentLap, _Atomic int ** ranks);

unsigned long long int getMemoryStats();

#endif