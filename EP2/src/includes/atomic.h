#ifndef ATOMIC_H
#define ATOMIC_H

#include <externals.h>

void lockPistaAt(int i, int j);

void unlockPistaAt(int i, int j);

void lockTrack(int i);

void unlockTrack(int i);

void mutexesInit(int distance);

void mutexesDestroy();

int pistaGet(int ** pista, int i, int j);

int trackGet(int track[], int i);

void pistaSet(int ** pista, int i, int j, int newValue);

void trackSet(int track[], int i, int acc);

#endif